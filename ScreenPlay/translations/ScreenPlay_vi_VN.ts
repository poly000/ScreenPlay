<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="vi_VN" sourcelanguage="en">
<context>
    <name>ColorPicker</name>
    <message>
        <source>Red</source>
        <translation>Đỏ</translation>
    </message>
    <message>
        <source>Green</source>
        <translation>Xanh lá</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation>Xanh dương</translation>
    </message>
    <message>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <source>HSV</source>
        <translation>HSV</translation>
    </message>
    <message>
        <source>R:</source>
        <translation>Đỏ:</translation>
    </message>
    <message>
        <source>G:</source>
        <translation>Xanh lá:</translation>
    </message>
    <message>
        <source>B:</source>
        <translation>Xanh dương:</translation>
    </message>
    <message>
        <source>H:</source>
        <translation>Màu:</translation>
    </message>
    <message>
        <source>S:</source>
        <translation>Độ bão hòa:</translation>
    </message>
    <message>
        <source>V:</source>
        <translation>Giá trị:</translation>
    </message>
    <message>
        <source>Alpha:</source>
        <translation>Độ trong suốt:</translation>
    </message>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
</context>
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Tin tức</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Diễn đàn</translation>
    </message>
    <message>
        <source>Issue List</source>
        <translation>D.sách lỗi</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Đóng góp</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Steam Workshop</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Mở trong trình duyệt</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Nhập bất kì loại video</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>Tùy thuộc vào cấu hình của PC mà việc chuyển đổi ảnh động của bạn sang một dạng video khác sẽ tốt hơn. Nếu cả hai đều có hiệu năng kém thì bạn có thể dùng một ảnh động QML!. Các loại video hỗ trợ:

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Chọn loại video mà bạn muốn:</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Chất lượng video: Giá trị thấp hơn đồng nghĩa với việc chất lượng cao hơn.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Mở tài liệu tham khảo</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Chọn tệp</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Đã có lỗi xảy ra!</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Sao chép vào khay nhớ tạm</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Quay trở lại việc tạo ảnh động và gửi một báo cáo lỗi!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Đang tạo ra ảnh xem trước...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Đang tạo ra hình thu nhỏ xem trước...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Đang tao ra video 5 giây xem trước...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Đang tạo ra gif xem trước...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Đang chuyển đổi dạng âm thanh...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Đang chuyển đổi dạng video... Việc này có thể tốn kha khá thời gian!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Đã có lỗi xảy ra khi chuyển đổi dạng video!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Đã có lỗi xảy ra khi đang xử lý video!</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Chuyển đổi một video sang ảnh động</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Đang tạo ra video xem trước...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Tên (bắt buộc!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Link YouTube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Hủy bỏ</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Lưu</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Lưu hình nền...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Âm lượng</translation>
    </message>
    <message>
        <source>Playback rate</source>
        <translation>Tốc độ phát lại</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Thời gian video hiện tại</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Cách lấp đầy</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Kéo dài</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Lấp đầy</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Chứa đựng</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Bao phủ</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>Giảm tỉ lệ</translation>
    </message>
</context>
<context>
    <name>FileSelector</name>
    <message>
        <source>Clear</source>
        <translation>Xóa</translation>
    </message>
    <message>
        <source>Select File</source>
        <translation>Chọn một tệp</translation>
    </message>
    <message>
        <source>Please choose a file</source>
        <translation>Xin hãy chọn một tệp</translation>
    </message>
</context>
<context>
    <name>Forum</name>
    <message>
        <source>Download Wallpaper and Widgets from our forums manually. If you want to download Steam Workshop content you have to install ScreenPlay via Steam.</source>
        <translation type="unfinished">Download Wallpaper and Widgets from our forums manually. If you want to download Steam Workshop content you have to install ScreenPlay via Steam.</translation>
    </message>
    <message>
        <source>Install Steam Version</source>
        <translation type="unfinished">Install Steam Version</translation>
    </message>
    <message>
        <source>Open In Browser</source>
        <translation type="unfinished">Open In Browser</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>Nhập một ảnh động gif</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>Thả một tệp *.gif vào đây hoặc nhấn nút &apos;Chọn một tệp&apos; ở dưới đây.</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>Chọn gif của bạn</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Tên ảnh động</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Thẻ</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>Tạo một ảnh động HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Tên ảnh động</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Được tạo bởi</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Bản quyền &amp; Thẻ</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Ảnh xem trước</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>Tạo một widget HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Tên widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Thẻ</translation>
    </message>
</context>
<context>
    <name>HeadlineSection</name>
    <message>
        <source>Headline Section</source>
        <translation>Phần tiêu đề</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <source>Set your own preview image</source>
        <translation>Đặt ảnh xem trước của bạn</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Xóa</translation>
    </message>
    <message>
        <source>Select Preview Image</source>
        <translation>Chọn ảnh xem trước</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>Đang xử lý video...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Đang tạo ra ảnh xem trước...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Đang tạo ra hình thu nhỏ xem trước...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Đang tao ra video 5 giây xem trước...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Đang tạo ra gif xem trước...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Đang chuyển đổi dạng âm thanh...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Đang chuyển đổi dạng video... Việc này có thể tốn kha khá thời gian!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Đã có lỗi xảy ra khi chuyển đổi dạng video!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Đã có lỗi xảy ra khi đang xử lý video!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Nhập một video vào hình nền</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Đang tạo ra video xem trước...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Tên (bắt buộc!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>Link YouTube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Hủy bỏ</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Lưu</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Lưu hình nền...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>Nhập một tệp webm</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>Khi mà nhập tệp webm chúng ta có thể bỏ qua quá trình chuyển đổi. Khi mà bạn có được kết quả không thỏa mãn với trình nhập tệp tất cả các loại video của ScreenPlay thì bạn cũng có thể sử dụng công cụ mã nguồn mở HandBrake!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>Loại tệp không hợp lệ. Bắt buộc phải là tệp VP8 hoặc VP9 hợp lệ (*.webm)!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>Thả một tệp *.webm vào đây hoặc nhấn nút &apos;Chọn một tệp&apos; ở dưới đây.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Mở tài liệu tham khảo</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Chọn một tệp</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished">Đang xử lý video...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished">Đang tạo ra ảnh xem trước...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Đang tạo ra hình thu nhỏ xem trước...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Đang tao ra video 5 giây xem trước...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Đang tạo ra gif xem trước...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished">Đang chuyển đổi dạng âm thanh...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Đang chuyển đổi dạng video... Việc này có thể tốn kha khá thời gian!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Đã có lỗi xảy ra khi chuyển đổi dạng video!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished">Đã có lỗi xảy ra khi đang xử lý video!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Nhập một video vào hình nền</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished">Đang tạo ra video xem trước...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished">Tên (bắt buộc!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Mô tả</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished">Link YouTube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished">Hủy bỏ</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Lưu</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Lưu hình nền...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished">Mở tài liệu tham khảo</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Đang làm mới!</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>Kéo xuống để làm mới!</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Lấy thêm nhiều hình nền &amp; widgets từ Steam Workshop!</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>Mở thư mục chứa hình nền.</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Xóa hình nền</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Xóa nhờ Workshop</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Mở trang workshop</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Bạn có chắc chắn muốn xóa hình nền này không?</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Lấy thêm nhiều hình nền &amp; widgets từ Steam Workshop</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Duyệt qua Steam Workshop</translation>
    </message>
</context>
<context>
    <name>LicenseSelector</name>
    <message>
        <source>License</source>
        <translation>Bản quyền</translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material for any purpose, even commercially.</source>
        <translation>Chia sẻ - sao chép và phân phối lại tài liệu ở bất kỳ phương tiện hoặc định dạng nào. Thích ứng - phối lại, biến đổi và xây dựng dựa trên chất liệu cho bất kỳ mục đích nào, kể cả về mặt thương mại. </translation>
    </message>
    <message>
        <source>You grant other to remix your work and change the license to their liking.</source>
        <translation type="unfinished">You grant other to remix your work and change the license to their liking.</translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material. You are not allowed to use it commercially! </source>
        <translation>Chia sẻ - sao chép và phân phối lại tài liệu ở bất kỳ phương tiện hoặc định dạng nào. Thích ứng - phối lại, chuyển đổi và xây dựng dựa trên chất liệu. Bạn không được phép sử dụng nó cho mục đích thương mại! </translation>
    </message>
    <message>
        <source>You allow everyone to do anything with your work.</source>
        <translation>Bạn cho phép mọi người làm bất cứ điều gì với thành quả của bạn. </translation>
    </message>
    <message>
        <source>You grant other to remix your work but it must remain under the GPLv3. We recommend this license for all code wallpaper!</source>
        <translation>Bạn cấp cho người khác để phối lại tác phẩm của bạn nhưng nó phải vẫn theo GPLv3. Chúng tôi đề xuất giấy phép này cho tất cả các hình nền mã!</translation>
    </message>
    <message>
        <source>You do not share any rights and nobody is allowed to use or remix it (Not recommended). Can also used to credit work others.</source>
        <translation>Bạn không chia sẻ bất kỳ quyền nào và không ai được phép sử dụng hoặc phối lại nó (Không khuyến khích). Cũng có thể được sử dụng để ghi thành quả của người khác.</translation>
    </message>
</context>
<context>
    <name>MonitorConfiguration</name>
    <message>
        <source>Your monitor setup changed!
 Please configure your wallpaper again.</source>
        <translation>Thiết lập màn hình của bạn đã thay đổi!
 Xin hãy thiết lập lại hình nền của bạn.</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>Cấu hình ảnh động</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Xóa mục đã chọn</translation>
    </message>
    <message>
        <source>Remove </source>
        <translation>Xóa</translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>Ảnh động</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>Đặt màu</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>Xin hãy chọn một màu</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>All</source>
        <translation>Tất cả</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation>Cảnh</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
    <message>
        <source>Install Date Ascending</source>
        <translation>Ngày cài đặt tăng dần</translation>
    </message>
    <message>
        <source>Install Date Descending</source>
        <translation>Ngày cài đặt giảm dần</translation>
    </message>
    <message>
        <source> Subscribed items: </source>
        <translation>Những mục đã đăng ký: </translation>
    </message>
    <message>
        <source>Upload to the Steam Workshop</source>
        <translation>Tải lên Steam Workshop</translation>
    </message>
    <message>
        <source>Create</source>
        <translation type="unfinished">Create</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation type="unfinished">Workshop</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="unfinished">Installed</translation>
    </message>
    <message>
        <source>Community</source>
        <translation type="unfinished">Community</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Settings</translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation>Thiết lập hình nền hoặc widgets đang hoạt động</translation>
    </message>
    <message>
        <source>No active Wallpaper or Widgets</source>
        <translation>Không có hình nền hoặc widgets đang hoạt động</translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <source>You need to run Steam for this. steamErrorRestart: %1 - steamErrorAPIInit: %2</source>
        <translation>Bạn cần chạy Steam để làm việc này ´｡• ω •｡` ♡ steamErrorRestart: %1 - steamErrorAPIInit: %2</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Quay lại</translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <source>You Need to Agree To The Steam Subscriber Agreement First</source>
        <translation>Bạn phải chấp nhận thỏa thuận người đăng ký của Steam trước</translation>
    </message>
    <message>
        <source>REQUIRES INTERNET CONNECTION AND FREE STEAM ACCOUNT TO ACTIVATE. Notice: Product offered subject to your acceptance of the Steam Subscriber Agreement (SSA). You must activate this product via the Internet by registering for a Steam account and accepting the SSA. Please see https://store.steampowered.com/subscriber_agreement/ to view the SSA prior to purchase. If you do not agree with the provisions of the SSA, you should return this game unopened to your retailer in accordance with their return policy.</source>
        <translation>YÊU CẦU KẾT NỐI INTERNET VÀ TÀI KHOẢN STEAM MIỄN PHÍ ĐỂ KÍCH HOẠT. Thông báo: Sản phẩm được cung cấp tùy thuộc vào việc bạn chấp nhận Thỏa thuận người đăng ký Steam (SSA). Bạn phải kích hoạt sản phẩm này qua Internet bằng cách đăng ký tài khoản Steam và chấp nhận SSA. Vui lòng xem https://store.steampowered.com/subscriber_agosystem/ để xem SSA trước khi mua. Nếu bạn không đồng ý với các quy định của SSA, bạn nên trả lại trò chơi chưa mở này cho nhà bán lẻ của bạn theo chính sách hoàn trả của họ.</translation>
    </message>
    <message>
        <source>View The Steam Subscriber Agreement</source>
        <translation>Xem thỏa thuận người đăng ký của Steam</translation>
    </message>
    <message>
        <source>Accept Steam Workshop Agreement</source>
        <translation>Chấp nhận thỏa thuận của Steam Workshop</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>Tạo một ảnh động QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Tên ảnh động</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Bản quyền &amp; Thẻ</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Ảnh xem trước</translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>Tạo một widget QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Tên widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Thẻ</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>Đã lưu cấu hình thành công!</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>MỚI</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation>Tìm ảnh động &amp; widgets</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Tự động mở</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay sẽ chạy cùng với Windows và sẽ thiết lập màn hình nền cho bạn.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>Tự động mở ưu tiên hơn</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>Tùy chọn này cho phép ScreenPlay quyền tự động chạy ưu tiên hơn những ứng dụng khác.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation>Gửi báo cáo sự cố và só liệu thống kê ẩn danh</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>Giúp chúng tôi làm ScreenPlay trở nên nhanh và ổn định hơn. Tất cả các dữ liệu thu thập được đều là ẩn danh và chỉ được sử dụng cho mục đích phát triển! Chúng tôi dùng &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; để thu thập và xử lý dữ liệu này. Một &lt;b&gt;sự cảm ơn lớn dành cho họ&lt;/b&gt; vì đã cung cấp cho chúng tôi bản trả phí miễn phí cho những dự án mã nguồn mở!</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation>Đặt vị trí lưu</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation>Đặt vị trí</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation>Đường dẫn lưu trữ của bạn đang trống!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>Quan trọng: Thay đổi thư mục này không có hiệu ứng gì ở thư mục tải về của workshop. ScreenPlay chỉ hỗ trợ có một thư mục chứa nội dung!</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Ngôn ngữ</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>Đặt ngôn ngữ của ScreenPlay</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Chủ đề</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>Chuyển chủ để sáng/tôí</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>Mặc định theo hệ thống</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Tối</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Sáng</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Hiệu suất</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>Tạm dừng ảnh nền video khi có một ứng dụng khác đang mở phía trước</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>Chúng tôi tắt hiển thị video (không phải âm thanh!) Để có hiệu suất tốt nhất. Nếu bạn gặp sự cố, bạn có thể vô hiệu hóa hành vi này tại đây. Yêu cầu khởi động lại hình nền!</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>Cách lấp đầy mặc định</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>Đặt thuộc tính này để xác định cách chia tỷ lệ video để phù hợp với khu vực mục tiêu.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Kéo dài</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Lấp đầy</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Chứa đựng</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Bao phủ</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Giảm tỉ lệ</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Về ứng dụng</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>Cảm ơn bạn vì đã sử dụng ScreenPlay</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>Chào, tôi là Elias Steurer hay được biết đến là Kelteseth và tôi là người phát triển của ScreenPlay. Cảm ơn bạn đã sử dụng phần mềm của tôi. Bạn có thể theo dõi tôi để nhận được những cập nhật về ScreenPlay tại:</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Phiên bản</translation>
    </message>
    <message>
        <source>ScreenPlay Build Version </source>
        <translation type="vanished">Bản dựng của ScreenPlay </translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation>Mở nhật kí thay đổi</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation>Phần mềm của bên thứ ba</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay sẽ không thể có được nếu như không có thành quả của những người khác. Một lời cảm ơn lớn đến: </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation>Bản quyền</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation>Nhật kí</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>Nếu ScreenPlay của bạn hoạt động sai thì đây là một cách tốt để tìm câu trả lời. Ở đây hiện tất cả các nhật kí và cảnh báo trong khi chạy</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation>Hiện nhật kí</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation>Bảo vệ dữ liệu</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>Chúng tôi sử dụng dữ liệu của bạn rất cẩn thận để cải thiện ScreenPlay. Chúng tôi không bán hoặc chia sẻ thông tin này (ẩn danh) với người khác!</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>Quyền riêng tư</translation>
    </message>
    <message>
        <source>ScreenPlay Build Version 
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Sao chép vào khay nhớ tạm</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation>Tổng quan về công cụ</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation>Hình nền GIF</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation>Hình nền QML</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation>Hình nền HTML5</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation>Trang chủ của hình nền</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation>Widget QML</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation>Widget HTML</translation>
    </message>
    <message>
        <source>Set Wallpaper</source>
        <translation>Đặt hình nền</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation>Đặt widget</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation>Tiêu đề</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation>Chọn một màn hình để hiển thị nội dung</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation>Chỉnh âm lượng</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Cách lấp đầy</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Kéo dài</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Lấp đầy</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Chứa đựng</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Bao phủ</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Giảm tỉ lệ</translation>
    </message>
    <message>
        <source>Size: </source>
        <translation>Kích cỡ: </translation>
    </message>
    <message>
        <source>No description...</source>
        <translation>Không có mô tả...</translation>
    </message>
    <message>
        <source>Click here if you like the content</source>
        <translation>Bấm vào đây nếu như bạn thích nội dung này</translation>
    </message>
    <message>
        <source>Click here if you do not like the content</source>
        <translation>Bấm vào đây nếu như bạn không thích nội dung này</translation>
    </message>
    <message>
        <source>Subscribtions: </source>
        <translation>Đăng ký: </translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation>Mở trong Steam</translation>
    </message>
    <message>
        <source>Subscribed!</source>
        <translation>Đã đăng kí!</translation>
    </message>
    <message>
        <source>Subscribe</source>
        <translation>Đăng kí</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SteamNotAvailable</name>
    <message>
        <source>Could not load steam integration!</source>
        <translation>Không thể tải tích hợp Steam!</translation>
    </message>
</context>
<context>
    <name>SteamProfile</name>
    <message>
        <source>Back</source>
        <translation>Quay lại</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Tiếp tục</translation>
    </message>
</context>
<context>
    <name>SteamWorkshopStartPage</name>
    <message>
        <source>Loading</source>
        <translation type="unfinished">Loading</translation>
    </message>
    <message>
        <source>Download now!</source>
        <translation type="unfinished">Download now!</translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation type="unfinished">Downloading...</translation>
    </message>
    <message>
        <source>Details</source>
        <translation type="unfinished">Details</translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation type="unfinished">Open In Steam</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation type="unfinished">Profile</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="unfinished">Upload</translation>
    </message>
    <message>
        <source>Search for Wallpaper and Widgets...</source>
        <translation type="unfinished">Search for Wallpaper and Widgets...</translation>
    </message>
    <message>
        <source>Open Workshop in Steam</source>
        <translation type="unfinished">Open Workshop in Steam</translation>
    </message>
    <message>
        <source>Ranked By Vote</source>
        <translation type="unfinished">Ranked By Vote</translation>
    </message>
    <message>
        <source>Publication Date</source>
        <translation type="unfinished">Publication Date</translation>
    </message>
    <message>
        <source>Ranked By Trend</source>
        <translation type="unfinished">Ranked By Trend</translation>
    </message>
    <message>
        <source>Favorited By Friends</source>
        <translation type="unfinished">Favorited By Friends</translation>
    </message>
    <message>
        <source>Created By Friends</source>
        <translation type="unfinished">Created By Friends</translation>
    </message>
    <message>
        <source>Created By Followed Users</source>
        <translation type="unfinished">Created By Followed Users</translation>
    </message>
    <message>
        <source>Not Yet Rated</source>
        <translation type="unfinished">Not Yet Rated</translation>
    </message>
    <message>
        <source>Total VotesAsc</source>
        <translation type="unfinished">Total VotesAsc</translation>
    </message>
    <message>
        <source>Votes Up</source>
        <translation type="unfinished">Votes Up</translation>
    </message>
    <message>
        <source>Total Unique Subscriptions</source>
        <translation type="unfinished">Total Unique Subscriptions</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="unfinished">Back</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="unfinished">Forward</translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <source>Save</source>
        <translation>Lưu</translation>
    </message>
    <message>
        <source>Add tag</source>
        <translation>Thêm thẻ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Hủy</translation>
    </message>
    <message>
        <source>Add Tag</source>
        <translation>Thêm thẻ</translation>
    </message>
</context>
<context>
    <name>TextField</name>
    <message>
        <source>Label</source>
        <translation>Nhãn mác</translation>
    </message>
    <message>
        <source>*Required</source>
        <translation>*Bắt buộc</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation>ScreenPlay - Bấm đúp để thay đổi cài đặt</translation>
    </message>
    <message>
        <source>Open ScreenPlay</source>
        <translation>Mở ScreenPlay</translation>
    </message>
    <message>
        <source>Mute all</source>
        <translation>Tắt tiếng tất cả hình nền</translation>
    </message>
    <message>
        <source>Unmute all</source>
        <translation>Bật tiếng tất cả hình nền</translation>
    </message>
    <message>
        <source>Pause all</source>
        <translation>Tạm dừng tất cả hình nền</translation>
    </message>
    <message>
        <source>Play all</source>
        <translation>Phát tất cả hình nền</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Thoát</translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation>Tải hình nền/widgets lên Steam</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Hủy bỏ</translation>
    </message>
    <message>
        <source>Upload Selected Projects</source>
        <translation>Tải lên dự án đã chọn</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation>Xong</translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <source>Type: </source>
        <translation>Loại: </translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation>Mở thư mục</translation>
    </message>
    <message>
        <source>Invalid Project!</source>
        <translation>Dự án không hợp lệ!</translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <source>Fail</source>
        <translation>Thất bại</translation>
    </message>
    <message>
        <source>No Connection</source>
        <translation>Không có kết nối</translation>
    </message>
    <message>
        <source>Invalid Password</source>
        <translation>Sai mật khẩu</translation>
    </message>
    <message>
        <source>Logged In Elsewhere</source>
        <translation>Đã được đăng nhập từ nơi khác</translation>
    </message>
    <message>
        <source>Invalid Protocol Version</source>
        <translation>Phiên bản giao thức không hợp lệ</translation>
    </message>
    <message>
        <source>Invalid Param</source>
        <translation>Tham số không hợp lệ</translation>
    </message>
    <message>
        <source>File Not Found</source>
        <translation>Không tìm thấy tập tin</translation>
    </message>
    <message>
        <source>Busy</source>
        <translation>Đang bận</translation>
    </message>
    <message>
        <source>Invalid State</source>
        <translation>Trạng thái không hợp lệ</translation>
    </message>
    <message>
        <source>Invalid Name</source>
        <translation>Tên không hợp lệ</translation>
    </message>
    <message>
        <source>Invalid Email</source>
        <translation>Email không hợp lệ</translation>
    </message>
    <message>
        <source>Duplicate Name</source>
        <translation>Tên bị trùng</translation>
    </message>
    <message>
        <source>Access Denied</source>
        <translation>Không có quyền truy cập</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Hết hạn</translation>
    </message>
    <message>
        <source>Banned</source>
        <translation>Đã bị cấm</translation>
    </message>
    <message>
        <source>Account Not Found</source>
        <translation>Không tìm thấy tài khoản</translation>
    </message>
    <message>
        <source>Invalid SteamID</source>
        <translation>ID Steam không hợp lệ</translation>
    </message>
    <message>
        <source>Service Unavailable</source>
        <translation>Dịch vụ không khả dụng</translation>
    </message>
    <message>
        <source>Not Logged On</source>
        <translation>Chưa đăng nhập</translation>
    </message>
    <message>
        <source>Pending</source>
        <translation>Đang chờ xử lý</translation>
    </message>
    <message>
        <source>Encryption Failure</source>
        <translation>Lỗi mã hóa</translation>
    </message>
    <message>
        <source>Insufficient Privilege</source>
        <translation>Không đủ quyền truy cập</translation>
    </message>
    <message>
        <source>Limit Exceeded</source>
        <translation>Quá giới hạn</translation>
    </message>
    <message>
        <source>Revoked</source>
        <translation>Đã thu hồi</translation>
    </message>
    <message>
        <source>Expired</source>
        <translation>Đã hết hạn</translation>
    </message>
    <message>
        <source>Already Redeemed</source>
        <translation>Đã quy đổi rồi</translation>
    </message>
    <message>
        <source>Duplicate Request</source>
        <translation>Yêu cầu bị trùng</translation>
    </message>
    <message>
        <source>Already Owned</source>
        <translation>Đã sở hữu</translation>
    </message>
    <message>
        <source>IP Not Found</source>
        <translation>Không tìm thấy IP</translation>
    </message>
    <message>
        <source>Persist Failed</source>
        <translation>Chờ đợi thất bại</translation>
    </message>
    <message>
        <source>Locking Failed</source>
        <translation>Khóa thất bại</translation>
    </message>
    <message>
        <source>Logon Session Replaced</source>
        <translation>Phiên đăng nhập đã được thay thế</translation>
    </message>
    <message>
        <source>Connect Failed</source>
        <translation>Kết nối thất bại</translation>
    </message>
    <message>
        <source>Handshake Failed</source>
        <translation>Handshake thất bại</translation>
    </message>
    <message>
        <source>IO Failure</source>
        <translation>Lỗi I/O</translation>
    </message>
    <message>
        <source>Remote Disconnect</source>
        <translation>Ngắt kết nối từ xa</translation>
    </message>
    <message>
        <source>Shopping Cart Not Found</source>
        <translation>Không tìm thấy giỏ hàng</translation>
    </message>
    <message>
        <source>Blocked</source>
        <translation>Đã bị chặn</translation>
    </message>
    <message>
        <source>Ignored</source>
        <translation>Đã bỏ qua</translation>
    </message>
    <message>
        <source>No Match</source>
        <translation>Không có kết quả nào phù hợp</translation>
    </message>
    <message>
        <source>Account Disabled</source>
        <translation>Tài khoản bị vô hiệu hóa</translation>
    </message>
    <message>
        <source>Service ReadOnly</source>
        <translation>Dịch vụ chỉ đọc</translation>
    </message>
    <message>
        <source>Account Not Featured</source>
        <translation>Tài khoản không nổi bật</translation>
    </message>
    <message>
        <source>Administrator OK</source>
        <translation>Quyền quản trị viên OK</translation>
    </message>
    <message>
        <source>Content Version</source>
        <translation>Phiên bản nội dung</translation>
    </message>
    <message>
        <source>Try Another CM</source>
        <translation>Hãy thử một cách khác</translation>
    </message>
    <message>
        <source>Password Required To Kick Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already Logged In Elsewhere</source>
        <translation>Đã được đăng nhập từ nơi khác</translation>
    </message>
    <message>
        <source>Suspended</source>
        <translation>Đã tạm ngưng</translation>
    </message>
    <message>
        <source>Cancelled</source>
        <translation>Đã hủy</translation>
    </message>
    <message>
        <source>Data Corruption</source>
        <translation>Dữ liệu bị hỏng</translation>
    </message>
    <message>
        <source>Disk Full</source>
        <translation>Ổ đĩa đầy</translation>
    </message>
    <message>
        <source>Remote Call Failed</source>
        <translation>Gọi từ xa không thành công</translation>
    </message>
    <message>
        <source>Password Unset</source>
        <translation>Mật khẩu chưa được đặt</translation>
    </message>
    <message>
        <source>External Account Unlinked</source>
        <translation>Tài khoản bên ngoài chưa được liên kết</translation>
    </message>
    <message>
        <source>PSN Ticket Invalid</source>
        <translation>Phiếu PSN không hợp lệ</translation>
    </message>
    <message>
        <source>External Account Already Linked</source>
        <translation>Tài khoản bên ngoài dã được liên kết rồi</translation>
    </message>
    <message>
        <source>Remote File Conflict</source>
        <translation>Tệp ở máy chủ bị xung đột</translation>
    </message>
    <message>
        <source>Illegal Password</source>
        <translation>Mật khẩu không hợp lệ</translation>
    </message>
    <message>
        <source>Same As Previous Value</source>
        <translation>Đặt giống như giá trị trước</translation>
    </message>
    <message>
        <source>Account Logon Denied</source>
        <translation>Đăng nhập bị từ chối</translation>
    </message>
    <message>
        <source>Cannot Use Old Password</source>
        <translation>Không thể sử dụng mật khẩu cũ</translation>
    </message>
    <message>
        <source>Invalid Login AuthCode</source>
        <translation>Mã xác minh đăng nhập không hợp lệ</translation>
    </message>
    <message>
        <source>Account Logon Denied No Mail</source>
        <translation>Đăng nhập thất bại, không có thư</translation>
    </message>
    <message>
        <source>Hardware Not Capable Of IPT</source>
        <translation>Phần cứng không hỗ trợ IPT</translation>
    </message>
    <message>
        <source>IPT Init Error</source>
        <translation>Lỗi bắt đầu IPT</translation>
    </message>
    <message>
        <source>Parental Control Restricted</source>
        <translation>Kiểm soát của phụ huynh bị hạn chế</translation>
    </message>
    <message>
        <source>Facebook Query Error</source>
        <translation>Kết nối Facebook lỗi</translation>
    </message>
    <message>
        <source>Expired Login Auth Code</source>
        <translation>Mã đăng nhập đã hết hạn</translation>
    </message>
    <message>
        <source>IP Login Restriction Failed</source>
        <translation>Hạn chế đăng nhập IP không thành công</translation>
    </message>
    <message>
        <source>Account Locked Down</source>
        <translation>Tài khoản đã bị khóa</translation>
    </message>
    <message>
        <source>Account Logon Denied Verified Email Required</source>
        <translation>Đăng nhập thất bại, yêu cầu xác thực mail</translation>
    </message>
    <message>
        <source>No MatchingURL</source>
        <translation>Không có địa chỉ nào phù hợp</translation>
    </message>
    <message>
        <source>Bad Response</source>
        <translation>Phản hồi xấu</translation>
    </message>
    <message>
        <source>Require Password ReEntry</source>
        <translation>Yêu cầu nhập lại mật khâu</translation>
    </message>
    <message>
        <source>Value Out Of Range</source>
        <translation>Giá trị quá vùng</translation>
    </message>
    <message>
        <source>Unexpecte Error</source>
        <translation>Lỗi Bất Thường</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>Đã vô hiệu</translation>
    </message>
    <message>
        <source>Invalid CEG Submission</source>
        <translation>Hồ sơ CEG không hợp lệ</translation>
    </message>
    <message>
        <source>Restricted Device</source>
        <translation>Thiết bị bị giới hạn</translation>
    </message>
    <message>
        <source>Region Locked</source>
        <translation>Vùng bị khóa</translation>
    </message>
    <message>
        <source>Rate Limit Exceeded</source>
        <translation>Vượt quá giới hạn truy cập</translation>
    </message>
    <message>
        <source>Account Login Denied Need Two Factor</source>
        <translation>Đăng nhập thất bại, cần 2FA</translation>
    </message>
    <message>
        <source>Item Deleted</source>
        <translation>Mặt hàng đã xóa</translation>
    </message>
    <message>
        <source>Account Login Denied Throttle</source>
        <translation>Đăng nhập thất bại</translation>
    </message>
    <message>
        <source>Two Factor Code Mismatch</source>
        <translation>Mã 2FA không đúng</translation>
    </message>
    <message>
        <source>Two Factor Activation Code Mismatch</source>
        <translation>Mã 2FA không đúng</translation>
    </message>
    <message>
        <source>Account Associated To Multiple Partners</source>
        <translation>Tài khoản được liên kết với nhiều đối tác</translation>
    </message>
    <message>
        <source>Not Modified</source>
        <translation>Chưa chỉnh sửa</translation>
    </message>
    <message>
        <source>No Mobile Device</source>
        <translation>Không có điện thoại</translation>
    </message>
    <message>
        <source>Time Not Synced</source>
        <translation>Chưa đồng bộ thời gian</translation>
    </message>
    <message>
        <source>Sms Code Failed</source>
        <translation>Gửi mã code SMS thất bại</translation>
    </message>
    <message>
        <source>Account Limit Exceeded</source>
        <translation>Đã vượt quá giới hạn tài khoản</translation>
    </message>
    <message>
        <source>Account Activity Limit Exceeded</source>
        <translation>Đã vượt quá giới hạn hoạt động của tài khoản</translation>
    </message>
    <message>
        <source>Phone Activity Limit Exceeded</source>
        <translation>Đã vượt quá giới hạn hoạt động của điện thoại</translation>
    </message>
    <message>
        <source>Refund To Wallet</source>
        <translation>Hoàn tiền vào ví</translation>
    </message>
    <message>
        <source>Email Send Failure</source>
        <translation>Gửi Thư Thất bại</translation>
    </message>
    <message>
        <source>Not Settled</source>
        <translation>Chưa được giải quyết</translation>
    </message>
    <message>
        <source>Need Captcha</source>
        <translation>Cần captcha</translation>
    </message>
    <message>
        <source>GSLT Denied</source>
        <translation>Mã đăng nhập máy chủ bị từ chối</translation>
    </message>
    <message>
        <source>GS Owner Denied</source>
        <translation>Máy chủ bị từ chối</translation>
    </message>
    <message>
        <source>Invalid Item Type</source>
        <translation>Kiểu mặt hàng không hợp lệ</translation>
    </message>
    <message>
        <source>IP Banned</source>
        <translation>IP đã bị cấm</translation>
    </message>
    <message>
        <source>GSLT Expired</source>
        <translation>Mã đăng nhập máy chủ đã hết hạn</translation>
    </message>
    <message>
        <source>Insufficient Funds</source>
        <translation>Không đủ tiền</translation>
    </message>
    <message>
        <source>Too Many Pending</source>
        <translation>Có quá nhiều mặt hàng đang chờ</translation>
    </message>
    <message>
        <source>No Site Licenses Found</source>
        <translation>Không tìm thấy giấy phép trang web</translation>
    </message>
    <message>
        <source>WG Network Send Exceeded</source>
        <translation>Đã vượt quá quá trình gửi mạng WG</translation>
    </message>
    <message>
        <source>Account Not Friends</source>
        <translation>Tài khoản không phải bạn bè</translation>
    </message>
    <message>
        <source>Limited User Account</source>
        <translation>Tài khoản bị giới hạn</translation>
    </message>
    <message>
        <source>Cant Remove Item</source>
        <translation>Không thể xóa mặt hàng</translation>
    </message>
    <message>
        <source>Account Deleted</source>
        <translation>Tài khoản đã bị xóa</translation>
    </message>
    <message>
        <source>Existing User Cancelled License</source>
        <translation>Người dùng hiện tại đã bị hủy giấy phép</translation>
    </message>
    <message>
        <source>Community Cooldown</source>
        <translation>Community Cooldown</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation>Trạng thái:</translation>
    </message>
    <message>
        <source>Upload Progress: </source>
        <translation>Quá trình tải lên: </translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation>Tạo một hình nền của trang web</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Chung</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Tên ảnh động</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Đươc tạo bởi</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Mô tả</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Thẻ</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Ảnh xem trước</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation>Lưu</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation>Đang lưu...</translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation>Đăng ký thành công!</translation>
    </message>
    <message>
        <source>Download complete!</source>
        <translation>Tải xuống thành công!</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation>Tin tức &amp; Ghi chú bản vá</translation>
    </message>
</context>
</TS>
