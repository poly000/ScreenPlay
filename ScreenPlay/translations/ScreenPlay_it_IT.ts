<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="en">
<context>
    <name>ColorPicker</name>
    <message>
        <source>Red</source>
        <translation>Rosso</translation>
    </message>
    <message>
        <source>Green</source>
        <translation>Verde</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation>Blu</translation>
    </message>
    <message>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <source>HSV</source>
        <translation>HSV</translation>
    </message>
    <message>
        <source>R:</source>
        <translation>R:</translation>
    </message>
    <message>
        <source>G:</source>
        <translation>G:</translation>
    </message>
    <message>
        <source>B:</source>
        <translation>B:</translation>
    </message>
    <message>
        <source>H:</source>
        <translation>H:</translation>
    </message>
    <message>
        <source>S:</source>
        <translation>S:</translation>
    </message>
    <message>
        <source>V:</source>
        <translation>V:</translation>
    </message>
    <message>
        <source>Alpha:</source>
        <translation>Trasparenza:</translation>
    </message>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
</context>
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Notizie</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message>
        <source>Issue List</source>
        <translation>Lista Problemi</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Contribuisci</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Steam Workshop</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Apri nel browser</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Importa qualsiasi tipo di video</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>A seconda della configurazione del tuo PC è meglio comvertire lo sfondo in un codec specifico. Se entrambi hanno scarse performance puoi provare uno sfondo QML! I formati video supportati sono:

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Imposta il tuo codec video preferito:</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Cursore per la qualità. A valori inferiori corrisponde una qualità migliore.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Apri documentazione</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Seleziona file</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Si è verificato un errore!</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Copia testo negli appunti</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Torna a creare e inviare un report di errore!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Generazione anteprima immagine...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Generazione miniatura di anteprima...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generazione anteprima video di 5 secondi...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generazione gif di anteprima...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Conversione Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Conversione Video... Potrebbe richiedere un po&apos; di tempo!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Conversione Video ERRORE!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Analisi Video ERRORE!</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Converti un video in uno sfondo</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generazione video di anteprima...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nome (obbligatorio)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>URL Youtube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Interrompi</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Salva sfondo...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <source>Playback rate</source>
        <translation>Velocità riproduzione</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Minutaggio</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Modalità Riempimento</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Estensione</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Riempimento schermo</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Proporzioni mantenute (barre nere)</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>Riduci</translation>
    </message>
</context>
<context>
    <name>FileSelector</name>
    <message>
        <source>Clear</source>
        <translation>Azzera</translation>
    </message>
    <message>
        <source>Select File</source>
        <translation>Seleziona file</translation>
    </message>
    <message>
        <source>Please choose a file</source>
        <translation>Scegli un file</translation>
    </message>
</context>
<context>
    <name>Forum</name>
    <message>
        <source>Download Wallpaper and Widgets from our forums manually. If you want to download Steam Workshop content you have to install ScreenPlay via Steam.</source>
        <translation>Scarica Wallpaper e Widget dai nostri forum manualmente. Se vuoi scaricare i contenuti da Steam Workshop devi installare ScreenPlay tramite Steam.</translation>
    </message>
    <message>
        <source>Install Steam Version</source>
        <translation>Installa Versione di Steam</translation>
    </message>
    <message>
        <source>Open In Browser</source>
        <translation>Apri nel browser</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>Importa uno sfondo Gif</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>Trascina un file *.gif qui o usa &apos;Seleziona file&apos; qui sotto.</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>Seleziona la tua gif</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nome sfondo</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tag</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>Crea uno sfondo HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nome sfondo</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Licenza &amp; Tag</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Immagine d’anteprima</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>Crea un Widget HTML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Nome Widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tag</translation>
    </message>
</context>
<context>
    <name>HeadlineSection</name>
    <message>
        <source>Headline Section</source>
        <translation>Sezione del Titolo</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <source>Set your own preview image</source>
        <translation>Imposta la tua immagine di anteprima</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Azzera</translation>
    </message>
    <message>
        <source>Select Preview Image</source>
        <translation>Seleziona Immagine d&apos;Anteprima</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>Analizza Video...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Generazione immagine d&apos;anteprima...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Generazione miniatura di anteprima...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generazione anteprima video di 5 secondi...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generazione gif di anteprima...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Conversione Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Conversione Video... Potrebbe richiedere un po&apos; di tempo!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Conversione Video ERRORE!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Analisi Video ERRORE!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Importa un video in uno sfondo</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generazione video di anteprima...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nome (obbligatorio)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>URL Youtube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Interrompi</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Salva sfondo...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>Importa un video .webm</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>Importando un file webm è possibile saltare la conversione. Se ottiene risultati insoddisfacenti dall&apos;importer di ScreenPlay &apos;Importa e converti video (tutti i tipi)&apos; puoi anche farlo con il convertitore gratuito ed open source HandBrake!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>Tipo di file non valido. Deve essere un valido VP8 o VP9 (*.webm)!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>Trascina un file *.webm qui o usa &apos;Seleziona file&apos; qui sotto.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Apri documentazione</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Seleziona file</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished">Analizza Video...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Generazione miniatura di anteprima...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Generazione anteprima video di 5 secondi...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Generazione gif di anteprima...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished">Conversione Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Conversione Video... Potrebbe richiedere un po&apos; di tempo!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Conversione Video ERRORE!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished">Analisi Video ERRORE!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Importa un video in uno sfondo</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished">Generazione video di anteprima...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished">Nome (obbligatorio)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Descrizione</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished">URL Youtube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished">Interrompi</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Salva</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Salva sfondo...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished">Apri documentazione</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished">Seleziona file</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Aggiornamento!</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>Trascina per aggiornare!</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Ottieni altri Wallpaper &amp; Widget dal workshop di Steam!</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>Apri percorso file</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Rimuovi elemento</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Rimuovi tramite Workshop</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Apri pagina del Workshop</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Sei sicuro di voler rimuovere questo elemento?</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Ottieni Widget e Wallpaper gratuiti tramite il Steam Workshop</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Sfoglia lo Steam Workshop</translation>
    </message>
</context>
<context>
    <name>LicenseSelector</name>
    <message>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material for any purpose, even commercially.</source>
        <translation>Condividere — copiare e ridistribuire il materiale in qualsiasi supporto o formato. Adattare — remixare, trasformare e sviluppare a partire dal materiale per qualsiasi scopo, anche commerciale.</translation>
    </message>
    <message>
        <source>You grant other to remix your work and change the license to their liking.</source>
        <translation>Tu concedi ad altri di modificare il tuo lavoro e modificare la licenza a loro piacimento.</translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material. You are not allowed to use it commercially! </source>
        <translation>Condividere — copiare e ridistribuire il materiale in qualsiasi supporto o formato. Adattare — modificare, trasformare e sviluppare a partire dal materiale per qualsiasi scopo, anche commerciale! </translation>
    </message>
    <message>
        <source>You allow everyone to do anything with your work.</source>
        <translation>Permetti a tutti di fare qualsiasi cosa con il tuo lavoro.</translation>
    </message>
    <message>
        <source>You grant other to remix your work but it must remain under the GPLv3. We recommend this license for all code wallpaper!</source>
        <translation>Tu concedi ad altri di modificare il tuo lavoro, ma deve rimanere sotto la GPLv3. Consigliamo questa licenza per tutti gli sfondi con codice!</translation>
    </message>
    <message>
        <source>You do not share any rights and nobody is allowed to use or remix it (Not recommended). Can also used to credit work others.</source>
        <translation>Non condividi alcun diritto e nessuno è autorizzato ad usarlo o modificarlo (Non raccomandato). Può anche essere usato per accreditare lavori di altri.</translation>
    </message>
</context>
<context>
    <name>MonitorConfiguration</name>
    <message>
        <source>Your monitor setup changed!
 Please configure your wallpaper again.</source>
        <translation>La configurazione del monitor è cambiata!
 Per favore configura di nuovo lo sfondo.</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>Configurazione Sfondo</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Rimuovi selezionati</translation>
    </message>
    <message>
        <source>Remove </source>
        <translation>Rimuovi </translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>Sfondi</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>Imposta colore</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>Scegli un colore</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>All</source>
        <translation>Tutto</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation>Scene</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
    <message>
        <source>Install Date Ascending</source>
        <translation>Data di Installazione Crescente</translation>
    </message>
    <message>
        <source>Install Date Descending</source>
        <translation>Data di Installazione Decrescente</translation>
    </message>
    <message>
        <source> Subscribed items: </source>
        <translation> Elementi sottoscritti: </translation>
    </message>
    <message>
        <source>Upload to the Steam Workshop</source>
        <translation>Carica nel Workshop di Steam</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Crea</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation>Workshop</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>Installati</translation>
    </message>
    <message>
        <source>Community</source>
        <translation>Comunità</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation>Configura Sfondi o Widgets attivi</translation>
    </message>
    <message>
        <source>No active Wallpaper or Widgets</source>
        <translation>Nessuno Sfondo o Widget attivi</translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <source>You need to run Steam for this. steamErrorRestart: %1 - steamErrorAPIInit: %2</source>
        <translation>È necessario eseguire Steam per questo. steamErrorRestart: %1 - steamErrorAPIInit: %2</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Indietro</translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <source>You Need to Agree To The Steam Subscriber Agreement First</source>
        <translation>Devi prima accettare l&apos;accordo di iscrizione di Steam</translation>
    </message>
    <message>
        <source>REQUIRES INTERNET CONNECTION AND FREE STEAM ACCOUNT TO ACTIVATE. Notice: Product offered subject to your acceptance of the Steam Subscriber Agreement (SSA). You must activate this product via the Internet by registering for a Steam account and accepting the SSA. Please see https://store.steampowered.com/subscriber_agreement/ to view the SSA prior to purchase. If you do not agree with the provisions of the SSA, you should return this game unopened to your retailer in accordance with their return policy.</source>
        <translation>RICHIESTA CONNESSIONE INTERNET E ACCOUNT STEAM GRATUITO PER L&apos;ATTIVAZIONE. Avviso: Prodotto offerto previa accettazione dell&apos;Accordo Iscrizione di Steam. È necessario attivare questo prodotto via Internet registrandosi per un account Steam e accettando il SSA. Per favore consulta https://store.steampowered.com/subscriber_agreement/ per visualizzare l&apos;SSA prima dell&apos;acquisto. Se non siete d&apos;accordo con le disposizioni della SSA, dovresti restituire questo gioco non aperto al tuo rivenditore in conformità con la sua politica di ritorno.</translation>
    </message>
    <message>
        <source>View The Steam Subscriber Agreement</source>
        <translation>Visualizza L&apos;Accordo Sull&apos;Iscrizione di Steam</translation>
    </message>
    <message>
        <source>Accept Steam Workshop Agreement</source>
        <translation>Accetta Accordo Workshop Steam</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>Crea uno sfondo QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nome sfondo</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Licenza &amp; Tag</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Immagine d’anteprima</translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>Crea un Widget QML</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Nome Widget</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tag</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>Prodilo salvato correttamente!</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>NUOVO</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation>Cerca sfondi &amp; widget</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Avvio automatico</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay si avvierà con Windows e configurerà il desktop ogni volta per te.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>Avvio automatico ad alta priorità</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>Questa opzione garantisce a ScreenPlay una priorità di avvio automatico più alta di altre applicazioni.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation>Invia report e statistiche anonime sui crash</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>Aiutaci a rendere ScreenPlay più veloce e stabile. Tutti i dati raccolti sono puramente anonimi e utilizzati solo per scopi di sviluppo! Usiamo &lt;a href=&quot;https://sentry.io&quot;&gt;sentry. o&lt;/a&gt; per raccogliere e analizzare questi dati. Un &lt;b&gt;grande grazie a loro&lt;/b&gt; per averci fornito supporto premium gratuito per i progetti open source!</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation>Imposta cartella di salvataggio</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation>Imposta posizione</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation>Il tuo percorso di archiviazione è vuoto!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>Importante: la modifica di questa cartella non ha effetto sul percorso di download del workshop. ScreenPlay supporta solo una cartella contenuti!</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>Imposta la lingua dell&apos;interfaccia utente di ScreenPlay</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>Cambia tema scuro/chiaro</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>Predefinito di Sistema</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Scuro</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Chiaro</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Prestazioni</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>Metti in pausa il rendering video dello sfondo mentre un&apos;altra app è in primo piano</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>Disattiviamo il rendering video (non l&apos;audio!) per migliori prestazioni. Se hai problemi puoi disabilitare questa opzione qui. È necessario riavviare lo sfondo!</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>Modalità riempimento predefinita</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>Imposta questa proprietà per definire come il video viene adattato all&apos;area di destinazione.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Estensione</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Riempimento</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Contenimento</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Coprente</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Riduzione</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Informazioni aggiuntive</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>Grazie per aver usato ScreenPlay</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>Ciao, sono Elias Steurer conosciuto anche come Kelteseth e sono lo sviluppatore di ScreenPlay. Grazie per aver utilizzato il mio software. Puoi seguirmi per ricevere aggiornamenti su ScreenPlay qui:</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
    <message>
        <source>ScreenPlay Build Version </source>
        <translation type="vanished">Versione Build di ScreenPlay </translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation>Apri Changelog</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation>Software di terze parti</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay non sarebbe possibile senza il lavoro di altri. Un grande ringraziamento a: </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation>Licenze</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation>Logs</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>Se il tuo ScreenPlay non funziona correttamente questo è un buon modo per cercare risposte. Questo mostra tutti i log e gli avvisi durante l&apos;esecuzione.</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation>Visualizza i Log</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation>Protezione dei dati</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>Utilizziamo i tuoi dati con molta attenzione per migliorare ScreenPlay. Non vendiamo o condividiamo queste informazioni (anonime) con altri!</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>Privacy</translation>
    </message>
    <message>
        <source>ScreenPlay Build Version 
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Copia testo negli appunti</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation>Panoramica Strumenti</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation>Sfondo GIF</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation>Sfondo QML</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation>Sfondo HTML5</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation>Sfondo Web</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation>Widget QML</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation>Widget HTML</translation>
    </message>
    <message>
        <source>Set Wallpaper</source>
        <translation>Imposta sfondo</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation>Imposta Widget</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation>Intestazione</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation>Seleziona un monitor per visualizzare il contenuto</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation>Imposta volume</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Modalità Riempimento</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Estensione</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Riempimento</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Proporzioni mantenute (barre nere)</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Rimpicciolisci</translation>
    </message>
    <message>
        <source>Size: </source>
        <translation>Dimensione: </translation>
    </message>
    <message>
        <source>No description...</source>
        <translation>Nessuna descrizione...</translation>
    </message>
    <message>
        <source>Click here if you like the content</source>
        <translation>Clicca qui se ti piace il contenuto</translation>
    </message>
    <message>
        <source>Click here if you do not like the content</source>
        <translation>Clicca qui se non ti piace il contenuto</translation>
    </message>
    <message>
        <source>Subscribtions: </source>
        <translation>Iscrizioni: </translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation>Apri In Steam</translation>
    </message>
    <message>
        <source>Subscribed!</source>
        <translation>Iscritto!</translation>
    </message>
    <message>
        <source>Subscribe</source>
        <translation>Iscriviti</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SteamNotAvailable</name>
    <message>
        <source>Could not load steam integration!</source>
        <translation>Impossibile caricare l&apos;integrazione di Steam!</translation>
    </message>
</context>
<context>
    <name>SteamProfile</name>
    <message>
        <source>Back</source>
        <translation>Indietro</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Avanti</translation>
    </message>
</context>
<context>
    <name>SteamWorkshopStartPage</name>
    <message>
        <source>Loading</source>
        <translation>Caricamento</translation>
    </message>
    <message>
        <source>Download now!</source>
        <translation>Scarica ora!</translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation>Download in corso...</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation>Apri In Steam</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profilo</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>Carica</translation>
    </message>
    <message>
        <source>Search for Wallpaper and Widgets...</source>
        <translation>Cerca sfondi &amp; widget...</translation>
    </message>
    <message>
        <source>Open Workshop in Steam</source>
        <translation>Apri Workshop in Steam</translation>
    </message>
    <message>
        <source>Ranked By Vote</source>
        <translation>Ordinati per voto</translation>
    </message>
    <message>
        <source>Publication Date</source>
        <translation>Data di pubblicazione</translation>
    </message>
    <message>
        <source>Ranked By Trend</source>
        <translation>Classificato Per Tendenza</translation>
    </message>
    <message>
        <source>Favorited By Friends</source>
        <translation>Preferiti da Amici</translation>
    </message>
    <message>
        <source>Created By Friends</source>
        <translation>Creati da Amici</translation>
    </message>
    <message>
        <source>Created By Followed Users</source>
        <translation>Creati da Utenti Seguiti</translation>
    </message>
    <message>
        <source>Not Yet Rated</source>
        <translation>Non Ancora Votati</translation>
    </message>
    <message>
        <source>Total VotesAsc</source>
        <translation>Totale Voti Asc</translation>
    </message>
    <message>
        <source>Votes Up</source>
        <translation type="unfinished">Votes Up</translation>
    </message>
    <message>
        <source>Total Unique Subscriptions</source>
        <translation type="unfinished">Total Unique Subscriptions</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Indietro</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Avanti</translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Add tag</source>
        <translation>Aggiungi tag</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Add Tag</source>
        <translation>Aggiungi tag</translation>
    </message>
</context>
<context>
    <name>TextField</name>
    <message>
        <source>Label</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <source>*Required</source>
        <translation>Obbligatorio</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation>ScreenPlay - Doppio clic per modificare le impostazioni.</translation>
    </message>
    <message>
        <source>Open ScreenPlay</source>
        <translation>Apri ScreenPlay</translation>
    </message>
    <message>
        <source>Mute all</source>
        <translation>Silenzia tutti</translation>
    </message>
    <message>
        <source>Unmute all</source>
        <translation>Desilenzia tutti</translation>
    </message>
    <message>
        <source>Pause all</source>
        <translation>Metti tutti in pausa</translation>
    </message>
    <message>
        <source>Play all</source>
        <translation>Riproduci tutti</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Esci</translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation>Carica Sfondo/Widget su Steam</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Interrompi</translation>
    </message>
    <message>
        <source>Upload Selected Projects</source>
        <translation>Carica Progetti Selezionati</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation>Termina</translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <source>Type: </source>
        <translation>Tipo: </translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation>Apri cartella</translation>
    </message>
    <message>
        <source>Invalid Project!</source>
        <translation>Progetto non valido!</translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <source>Fail</source>
        <translation>Errore</translation>
    </message>
    <message>
        <source>No Connection</source>
        <translation>Nessuna connessione</translation>
    </message>
    <message>
        <source>Invalid Password</source>
        <translation>Password non valida</translation>
    </message>
    <message>
        <source>Logged In Elsewhere</source>
        <translation>Già collegato altrove</translation>
    </message>
    <message>
        <source>Invalid Protocol Version</source>
        <translation>Versione Protocollo Non Valida</translation>
    </message>
    <message>
        <source>Invalid Param</source>
        <translation>Parametro non valido</translation>
    </message>
    <message>
        <source>File Not Found</source>
        <translation>File non trovato</translation>
    </message>
    <message>
        <source>Busy</source>
        <translation>Occupato</translation>
    </message>
    <message>
        <source>Invalid State</source>
        <translation>Stato non valido</translation>
    </message>
    <message>
        <source>Invalid Name</source>
        <translation>Nome non valido</translation>
    </message>
    <message>
        <source>Invalid Email</source>
        <translation>Indirizzo e-mail non valido</translation>
    </message>
    <message>
        <source>Duplicate Name</source>
        <translation>Nome Duplicato</translation>
    </message>
    <message>
        <source>Access Denied</source>
        <translation>Accesso negato</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Tempo Scaduto</translation>
    </message>
    <message>
        <source>Banned</source>
        <translation>Bannato</translation>
    </message>
    <message>
        <source>Account Not Found</source>
        <translation>Account non trovato</translation>
    </message>
    <message>
        <source>Invalid SteamID</source>
        <translation>SteamID Non Valido</translation>
    </message>
    <message>
        <source>Service Unavailable</source>
        <translation>Servizio non Disponibile</translation>
    </message>
    <message>
        <source>Not Logged On</source>
        <translation>Accesso non effettuato</translation>
    </message>
    <message>
        <source>Pending</source>
        <translation>In attesa</translation>
    </message>
    <message>
        <source>Encryption Failure</source>
        <translation>Errore Di Cifratura</translation>
    </message>
    <message>
        <source>Insufficient Privilege</source>
        <translation>Permessi insufficienti</translation>
    </message>
    <message>
        <source>Limit Exceeded</source>
        <translation>Limite superato</translation>
    </message>
    <message>
        <source>Revoked</source>
        <translation>Revocato</translation>
    </message>
    <message>
        <source>Expired</source>
        <translation>Scaduto</translation>
    </message>
    <message>
        <source>Already Redeemed</source>
        <translation>Già Riscattato</translation>
    </message>
    <message>
        <source>Duplicate Request</source>
        <translation>Richiesta Duplicata</translation>
    </message>
    <message>
        <source>Already Owned</source>
        <translation>Già Posseduto</translation>
    </message>
    <message>
        <source>IP Not Found</source>
        <translation>IP Non Trovato</translation>
    </message>
    <message>
        <source>Persist Failed</source>
        <translation type="unfinished">Persist Failed</translation>
    </message>
    <message>
        <source>Locking Failed</source>
        <translation>Blocco non riuscito</translation>
    </message>
    <message>
        <source>Logon Session Replaced</source>
        <translation>Sessione Di Accesso Sostituita</translation>
    </message>
    <message>
        <source>Connect Failed</source>
        <translation>Connessione fallita</translation>
    </message>
    <message>
        <source>Handshake Failed</source>
        <translation>Handshake Fallito</translation>
    </message>
    <message>
        <source>IO Failure</source>
        <translation>Errore IO</translation>
    </message>
    <message>
        <source>Remote Disconnect</source>
        <translation>Disconnessione Remota</translation>
    </message>
    <message>
        <source>Shopping Cart Not Found</source>
        <translation>Carrello Non Trovato</translation>
    </message>
    <message>
        <source>Blocked</source>
        <translation>Bloccato</translation>
    </message>
    <message>
        <source>Ignored</source>
        <translation>Ignorato</translation>
    </message>
    <message>
        <source>No Match</source>
        <translation>Nessun Risultato</translation>
    </message>
    <message>
        <source>Account Disabled</source>
        <translation>Account disabilitato</translation>
    </message>
    <message>
        <source>Service ReadOnly</source>
        <translation>Servizio Sola Lettura</translation>
    </message>
    <message>
        <source>Account Not Featured</source>
        <translation>Account Non In Evidenza</translation>
    </message>
    <message>
        <source>Administrator OK</source>
        <translation>Amministratore OK</translation>
    </message>
    <message>
        <source>Content Version</source>
        <translation>Versione Contenuto</translation>
    </message>
    <message>
        <source>Try Another CM</source>
        <translation>Prova un altro CM</translation>
    </message>
    <message>
        <source>Password Required To Kick Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already Logged In Elsewhere</source>
        <translation>Già collegato altrove</translation>
    </message>
    <message>
        <source>Suspended</source>
        <translation>Sospeso</translation>
    </message>
    <message>
        <source>Cancelled</source>
        <translation>Annullato</translation>
    </message>
    <message>
        <source>Data Corruption</source>
        <translation>Dati corrotti</translation>
    </message>
    <message>
        <source>Disk Full</source>
        <translation>Disco pieno</translation>
    </message>
    <message>
        <source>Remote Call Failed</source>
        <translation>Chiamata Remota Non Riuscita</translation>
    </message>
    <message>
        <source>Password Unset</source>
        <translation>Password non impostata</translation>
    </message>
    <message>
        <source>External Account Unlinked</source>
        <translation>Account Esterno Non Collegato</translation>
    </message>
    <message>
        <source>PSN Ticket Invalid</source>
        <translation type="unfinished">PSN Ticket Invalid</translation>
    </message>
    <message>
        <source>External Account Already Linked</source>
        <translation>Account Esterno Già Collegato</translation>
    </message>
    <message>
        <source>Remote File Conflict</source>
        <translation>Conflitto File Remoto</translation>
    </message>
    <message>
        <source>Illegal Password</source>
        <translation>Password non valida</translation>
    </message>
    <message>
        <source>Same As Previous Value</source>
        <translation>Valore uguale al precedente</translation>
    </message>
    <message>
        <source>Account Logon Denied</source>
        <translation>Accesso Account Negato</translation>
    </message>
    <message>
        <source>Cannot Use Old Password</source>
        <translation>Impossibile Usare Vecchia Password</translation>
    </message>
    <message>
        <source>Invalid Login AuthCode</source>
        <translation type="unfinished">Invalid Login AuthCode</translation>
    </message>
    <message>
        <source>Account Logon Denied No Mail</source>
        <translation>Accesso Account Negato Nessuna Mail</translation>
    </message>
    <message>
        <source>Hardware Not Capable Of IPT</source>
        <translation>Hardware non compatibile con IPT</translation>
    </message>
    <message>
        <source>IPT Init Error</source>
        <translation>Errore Init IPT</translation>
    </message>
    <message>
        <source>Parental Control Restricted</source>
        <translation>Limitato da Parental Control</translation>
    </message>
    <message>
        <source>Facebook Query Error</source>
        <translation type="unfinished">Facebook Query Error</translation>
    </message>
    <message>
        <source>Expired Login Auth Code</source>
        <translation type="unfinished">Expired Login Auth Code</translation>
    </message>
    <message>
        <source>IP Login Restriction Failed</source>
        <translation type="unfinished">IP Login Restriction Failed</translation>
    </message>
    <message>
        <source>Account Locked Down</source>
        <translation>Account bloccato</translation>
    </message>
    <message>
        <source>Account Logon Denied Verified Email Required</source>
        <translation>Accesso Account Negato Verifica Email Richiesta</translation>
    </message>
    <message>
        <source>No MatchingURL</source>
        <translation>Nessun URL Corrispondente</translation>
    </message>
    <message>
        <source>Bad Response</source>
        <translation type="unfinished">Bad Response</translation>
    </message>
    <message>
        <source>Require Password ReEntry</source>
        <translation>Richiesta Reinserimento Password</translation>
    </message>
    <message>
        <source>Value Out Of Range</source>
        <translation>Valore fuori dall&apos;intervallo</translation>
    </message>
    <message>
        <source>Unexpecte Error</source>
        <translation>Errore inatteso</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>Disabilitato</translation>
    </message>
    <message>
        <source>Invalid CEG Submission</source>
        <translation type="unfinished">Invalid CEG Submission</translation>
    </message>
    <message>
        <source>Restricted Device</source>
        <translation>Dispositivo Limitato</translation>
    </message>
    <message>
        <source>Region Locked</source>
        <translation>Blocco regionale</translation>
    </message>
    <message>
        <source>Rate Limit Exceeded</source>
        <translation>Limite di richieste superato</translation>
    </message>
    <message>
        <source>Account Login Denied Need Two Factor</source>
        <translation>Login Account Negato Necessaria Autenticazione Due Fattori</translation>
    </message>
    <message>
        <source>Item Deleted</source>
        <translation>Elemento eliminato</translation>
    </message>
    <message>
        <source>Account Login Denied Throttle</source>
        <translation type="unfinished">Account Login Denied Throttle</translation>
    </message>
    <message>
        <source>Two Factor Code Mismatch</source>
        <translation>Codice A Due Fattori Non Corrispondente</translation>
    </message>
    <message>
        <source>Two Factor Activation Code Mismatch</source>
        <translation>Codice Di Attivazione A Due Fattori Non Corrispondente</translation>
    </message>
    <message>
        <source>Account Associated To Multiple Partners</source>
        <translation>Account Associato A Partner Multipli</translation>
    </message>
    <message>
        <source>Not Modified</source>
        <translation>Non modificato</translation>
    </message>
    <message>
        <source>No Mobile Device</source>
        <translation>Nessun Dispositivo Mobile</translation>
    </message>
    <message>
        <source>Time Not Synced</source>
        <translation>Tempo Non Sincronizzato</translation>
    </message>
    <message>
        <source>Sms Code Failed</source>
        <translation>Codice Sms Fallito</translation>
    </message>
    <message>
        <source>Account Limit Exceeded</source>
        <translation>Limite Account Superato</translation>
    </message>
    <message>
        <source>Account Activity Limit Exceeded</source>
        <translation>Limite Attività Dell&apos;Account Superato</translation>
    </message>
    <message>
        <source>Phone Activity Limit Exceeded</source>
        <translation>Limita Attività Telefono Superato</translation>
    </message>
    <message>
        <source>Refund To Wallet</source>
        <translation>Rimborso Al Portafoglio</translation>
    </message>
    <message>
        <source>Email Send Failure</source>
        <translation>Errore nell&apos;invio della mail</translation>
    </message>
    <message>
        <source>Not Settled</source>
        <translation>Non Risolto</translation>
    </message>
    <message>
        <source>Need Captcha</source>
        <translation>Captcha Richiesto</translation>
    </message>
    <message>
        <source>GSLT Denied</source>
        <translation>GSLT Negato</translation>
    </message>
    <message>
        <source>GS Owner Denied</source>
        <translation>Proprietario GS Negato</translation>
    </message>
    <message>
        <source>Invalid Item Type</source>
        <translation>Tipo oggetto non valido</translation>
    </message>
    <message>
        <source>IP Banned</source>
        <translation>IP Bannato</translation>
    </message>
    <message>
        <source>GSLT Expired</source>
        <translation>GSLT Scaduto</translation>
    </message>
    <message>
        <source>Insufficient Funds</source>
        <translation>Fondi insufficienti</translation>
    </message>
    <message>
        <source>Too Many Pending</source>
        <translation>Troppi In Attesa</translation>
    </message>
    <message>
        <source>No Site Licenses Found</source>
        <translation>Nessuna Licenza Sito Trovata</translation>
    </message>
    <message>
        <source>WG Network Send Exceeded</source>
        <translation>Invii di rete WG Superati</translation>
    </message>
    <message>
        <source>Account Not Friends</source>
        <translation>Account non in Amici</translation>
    </message>
    <message>
        <source>Limited User Account</source>
        <translation>Account Limitato</translation>
    </message>
    <message>
        <source>Cant Remove Item</source>
        <translation>Impossibile rimuovere elemento</translation>
    </message>
    <message>
        <source>Account Deleted</source>
        <translation>Account eliminato</translation>
    </message>
    <message>
        <source>Existing User Cancelled License</source>
        <translation>Licenza annullata da utente esistente</translation>
    </message>
    <message>
        <source>Community Cooldown</source>
        <translation>Cooldown comunità</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation>Stato:</translation>
    </message>
    <message>
        <source>Upload Progress: </source>
        <translation>Avanzamento del caricamento: </translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation>Crea uno sfondo web</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Nome sfondo</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Creato da</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Tag</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Immagine d’anteprima</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation>Salvataggio in corso...</translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation>Iscritto con successo all&apos;oggetto Workshop!</translation>
    </message>
    <message>
        <source>Download complete!</source>
        <translation>Download completato!</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation>Notizie &amp; Patchnotes</translation>
    </message>
</context>
</TS>
