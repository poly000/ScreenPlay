<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ColorPicker</name>
    <message>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>RGB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>R:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>G:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>B:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>H:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>S:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>V:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alpha:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Issue List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Playback rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileSelector</name>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please choose a file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Forum</name>
    <message>
        <source>Download Wallpaper and Widgets from our forums manually. If you want to download Steam Workshop content you have to install ScreenPlay via Steam.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Install Steam Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open In Browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeadlineSection</name>
    <message>
        <source>Headline Section</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <source>Set your own preview image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select Preview Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LicenseSelector</name>
    <message>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material for any purpose, even commercially.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You grant other to remix your work and change the license to their liking.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material. You are not allowed to use it commercially! </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You allow everyone to do anything with your work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You grant other to remix your work but it must remain under the GPLv3. We recommend this license for all code wallpaper!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You do not share any rights and nobody is allowed to use or remix it (Not recommended). Can also used to credit work others.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MonitorConfiguration</name>
    <message>
        <source>Your monitor setup changed!
 Please configure your wallpaper again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Install Date Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Install Date Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Subscribed items: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload to the Steam Workshop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Community</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No active Wallpaper or Widgets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <source>You need to run Steam for this. steamErrorRestart: %1 - steamErrorAPIInit: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <source>You Need to Agree To The Steam Subscriber Agreement First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>REQUIRES INTERNET CONNECTION AND FREE STEAM ACCOUNT TO ACTIVATE. Notice: Product offered subject to your acceptance of the Steam Subscriber Agreement (SSA). You must activate this product via the Internet by registering for a Steam account and accepting the SSA. Please see https://store.steampowered.com/subscriber_agreement/ to view the SSA prior to purchase. If you do not agree with the provisions of the SSA, you should return this game unopened to your retailer in accordance with their return policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View The Steam Subscriber Agreement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Accept Steam Workshop Agreement</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Performance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenPlay Build Version 
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Headline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No description...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click here if you like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click here if you do not like the content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subscribtions: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subscribed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SteamNotAvailable</name>
    <message>
        <source>Could not load steam integration!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SteamProfile</name>
    <message>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SteamWorkshopStartPage</name>
    <message>
        <source>Loading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download now!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search for Wallpaper and Widgets...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Workshop in Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ranked By Vote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Publication Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ranked By Trend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Favorited By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created By Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created By Followed Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not Yet Rated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total VotesAsc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Votes Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Total Unique Subscriptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add tag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add Tag</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TextField</name>
    <message>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>*Required</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open ScreenPlay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mute all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unmute all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pause all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Play all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload Selected Projects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <source>Type: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid Project!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <source>Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid Protocol Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid Param</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Duplicate Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Access Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid SteamID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Service Unavailable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not Logged On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Encryption Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insufficient Privilege</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Revoked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already Redeemed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Duplicate Request</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already Owned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IP Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Persist Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Locking Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logon Session Replaced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connect Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Handshake Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IO Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remote Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Shopping Cart Not Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Blocked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ignored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Service ReadOnly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Not Featured</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Administrator OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Content Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Try Another CM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password Required To Kick Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already Logged In Elsewhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Suspended</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancelled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Data Corruption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disk Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remote Call Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password Unset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>External Account Unlinked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>PSN Ticket Invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>External Account Already Linked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remote File Conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Illegal Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Same As Previous Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Logon Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot Use Old Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid Login AuthCode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Logon Denied No Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hardware Not Capable Of IPT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IPT Init Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Parental Control Restricted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Facebook Query Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expired Login Auth Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IP Login Restriction Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Locked Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Logon Denied Verified Email Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No MatchingURL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bad Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Require Password ReEntry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Value Out Of Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unexpecte Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid CEG Submission</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restricted Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Region Locked</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rate Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Login Denied Need Two Factor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Item Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Login Denied Throttle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Two Factor Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Two Factor Activation Code Mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Associated To Multiple Partners</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Mobile Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time Not Synced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sms Code Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone Activity Limit Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refund To Wallet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email Send Failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Not Settled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Need Captcha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GSLT Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GS Owner Denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid Item Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>IP Banned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GSLT Expired</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Insufficient Funds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Too Many Pending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Site Licenses Found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>WG Network Send Exceeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Not Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Limited User Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cant Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account Deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Existing User Cancelled License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Community Cooldown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Upload Progress: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download complete!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
