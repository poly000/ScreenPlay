<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>ColorPicker</name>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="345"/>
        <source>Red</source>
        <translation>红</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="364"/>
        <source>Green</source>
        <translation>绿</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="383"/>
        <source>Blue</source>
        <translation>蓝</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="572"/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="590"/>
        <source>HSV</source>
        <translation>HSV</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="617"/>
        <source>R:</source>
        <translation>R:</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="634"/>
        <source>G:</source>
        <translation>G:</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="651"/>
        <source>B:</source>
        <translation>B:</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="668"/>
        <source>H:</source>
        <translation>H:</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="685"/>
        <source>S:</source>
        <translation>S:</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="702"/>
        <source>V:</source>
        <translation>V:</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="723"/>
        <source>Alpha:</source>
        <translation>透明度:</translation>
    </message>
    <message>
        <location filename="../qml/Common/ColorPicker.qml" line="740"/>
        <source>#</source>
        <translation>#</translation>
    </message>
</context>
<context>
    <name>Community</name>
    <message>
        <location filename="../qml/Community/Community.qml" line="38"/>
        <source>News</source>
        <translation>新闻</translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="44"/>
        <source>Wiki</source>
        <translation>维基</translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="50"/>
        <source>Forum</source>
        <translation>论坛</translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="56"/>
        <source>Issue List</source>
        <translation>议题列表</translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="62"/>
        <source>Contribute</source>
        <translation>贡献</translation>
    </message>
    <message>
        <location filename="../qml/Community/Community.qml" line="68"/>
        <source>Steam Workshop</source>
        <translation>Steam 创意工坊</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <location filename="../qml/Community/CommunityNavItem.qml" line="57"/>
        <source>Open in browser</source>
        <translation>在浏览器中打开</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperInit.qml" line="32"/>
        <source>Import any video type</source>
        <translation>导入任何视频类型</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperInit.qml" line="38"/>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>取决于您的PC配置，最好将您的壁纸转换为特定的视频编码格式。如果它们的性能都不好，您还可以尝试 QML 壁纸！已支持的视频格式有：

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperInit.qml" line="53"/>
        <source>Set your preffered video codec:</source>
        <translation>设置您偏好的视频编码格式：</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperInit.qml" line="98"/>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>质量滑条，更小的值意味着更好的质量。</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperInit.qml" line="113"/>
        <source>Open Documentation</source>
        <translation>打开文档</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperInit.qml" line="132"/>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperResult.qml" line="18"/>
        <source>An error occurred!</source>
        <translation>发生错误！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperResult.qml" line="91"/>
        <source>Copy text to clipboard</source>
        <translation>复制到剪贴板</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperResult.qml" line="102"/>
        <source>Back to create and send an error report!</source>
        <translation>返回以创建并发送错误报告！</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="48"/>
        <source>Generating preview image...</source>
        <translation>生成预览图...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="51"/>
        <source>Generating preview thumbnail image...</source>
        <translation>生成预览缩略图...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="58"/>
        <source>Generating 5 second preview video...</source>
        <translation>生成5秒预览视频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="61"/>
        <source>Generating preview gif...</source>
        <translation>生成预览GIF...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="70"/>
        <source>Converting Audio...</source>
        <translation>转换音频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="73"/>
        <source>Converting Video... This can take some time!</source>
        <translation>转换视频... 这可能需要一些时间！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="77"/>
        <source>Converting Video ERROR!</source>
        <translation>转换视频出错！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="80"/>
        <source>Analyse Video ERROR!</source>
        <translation>分析视频出错！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="109"/>
        <source>Convert a video to a wallpaper</source>
        <translation>将视频转换为壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="219"/>
        <source>Generating preview video...</source>
        <translation>生成预览视频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="275"/>
        <source>Name (required!)</source>
        <translation>名称（必选）</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="289"/>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="297"/>
        <source>Youtube URL</source>
        <translation>Youtube 链接</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="327"/>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="340"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportVideoAndConvert/CreateWallpaperVideoImportConvert.qml" line="377"/>
        <source>Save Wallpaper...</source>
        <translation>保存壁纸...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="41"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="54"/>
        <source>Playback rate</source>
        <translation>播放速度</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="66"/>
        <source>Current Video Time</source>
        <translation>目前视频时间</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="83"/>
        <source>Fill Mode</source>
        <translation>填充模式</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="102"/>
        <source>Stretch</source>
        <translation>拉伸</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="105"/>
        <source>Fill</source>
        <translation>填充</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="108"/>
        <source>Contain</source>
        <translation>适应</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="111"/>
        <source>Cover</source>
        <translation>平铺</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/DefaultVideoControls.qml" line="114"/>
        <source>Scale_Down</source>
        <translation>裁切</translation>
    </message>
</context>
<context>
    <name>FileSelector</name>
    <message>
        <location filename="../qml/Common/FileSelector.qml" line="102"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../qml/Common/FileSelector.qml" line="122"/>
        <source>Select File</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../qml/Common/FileSelector.qml" line="141"/>
        <source>Please choose a file</source>
        <translation>请选择文件</translation>
    </message>
</context>
<context>
    <name>Forum</name>
    <message>
        <location filename="../qml/Workshop/Forum.qml" line="35"/>
        <source>Download Wallpaper and Widgets from our forums manually. If you want to download Steam Workshop content you have to install ScreenPlay via Steam.</source>
        <translation>从我们的社区手动下载壁纸与部件 如果您想要下载Steam创意工坊内容，您须要通过Steam安装ScreemPlay。</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Forum.qml" line="44"/>
        <source>Install Steam Version</source>
        <translation>安装Steam版本</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Forum.qml" line="48"/>
        <source>Open In Browser</source>
        <translation>浏览器中打开</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <location filename="../qml/Create/Wizards/GifWallpaper.qml" line="25"/>
        <source>Import a Gif Wallpaper</source>
        <translation>导入 GIF 壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/GifWallpaper.qml" line="81"/>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>拖放一个 *.gif 文件到这里，或者使用下面的 &apos;选择文件&apos;。</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/GifWallpaper.qml" line="111"/>
        <source>Select your gif</source>
        <translation>选择您的 GIF</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/GifWallpaper.qml" line="126"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/GifWallpaper.qml" line="133"/>
        <source>Wallpaper name</source>
        <translation>壁纸名称</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/GifWallpaper.qml" line="141"/>
        <source>Created By</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/GifWallpaper.qml" line="151"/>
        <source>Tags</source>
        <translation>标签</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWallpaper.qml" line="29"/>
        <source>Create a HTML Wallpaper</source>
        <translation>创建 HTML 壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWallpaper.qml" line="34"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWallpaper.qml" line="44"/>
        <source>Wallpaper name</source>
        <translation>壁纸名称</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWallpaper.qml" line="53"/>
        <source>Created By</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWallpaper.qml" line="62"/>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWallpaper.qml" line="70"/>
        <source>License &amp; Tags</source>
        <translation>许可证 &amp; 标签</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWallpaper.qml" line="93"/>
        <source>Preview Image</source>
        <translation>预览图</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWidget.qml" line="20"/>
        <source>Create a HTML widget</source>
        <translation>创建 HTML 部件</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWidget.qml" line="74"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWidget.qml" line="82"/>
        <source>Widget name</source>
        <translation>部件名称</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWidget.qml" line="90"/>
        <source>Created by</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/HTMLWidget.qml" line="98"/>
        <source>Tags</source>
        <translation>标签</translation>
    </message>
</context>
<context>
    <name>HeadlineSection</name>
    <message>
        <location filename="../qml/Common/HeadlineSection.qml" line="6"/>
        <source>Headline Section</source>
        <translation>摘要</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="118"/>
        <source>Set your own preview image</source>
        <translation>设置您的预览图</translation>
    </message>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="159"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="../qml/Common/ImageSelector.qml" line="176"/>
        <source>Select Preview Image</source>
        <translation>选择预览图</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="43"/>
        <source>AnalyseVideo...</source>
        <translation>分析视频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="46"/>
        <source>Generating preview image...</source>
        <translation>生成预览图...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="49"/>
        <source>Generating preview thumbnail image...</source>
        <translation>生成预览缩略图...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="56"/>
        <source>Generating 5 second preview video...</source>
        <translation>生成5秒预览视频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="59"/>
        <source>Generating preview gif...</source>
        <translation>生成预览 GIF...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="68"/>
        <source>Converting Audio...</source>
        <translation>转换音频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="71"/>
        <source>Converting Video... This can take some time!</source>
        <translation>转换视频... 这可能需要一些时间！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="74"/>
        <source>Converting Video ERROR!</source>
        <translation>转换视频出错！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="77"/>
        <source>Analyse Video ERROR!</source>
        <translation>分析视频出错！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="106"/>
        <source>Import a video to a wallpaper</source>
        <translation>将视频导入为壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="215"/>
        <source>Generating preview video...</source>
        <translation>生成预览视频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="275"/>
        <source>Name (required!)</source>
        <translation>名称（必选）</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="289"/>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="297"/>
        <source>Youtube URL</source>
        <translation>Youtube 链接</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="328"/>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="341"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmConvert.qml" line="375"/>
        <source>Save Wallpaper...</source>
        <translation>保存壁纸...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmInit.qml" line="32"/>
        <source>Import a .webm video</source>
        <translation>导入 .webm 视频</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmInit.qml" line="48"/>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>导入webm时，我们可以跳过漫长的转换过程。当您对&apos;导入和转换（所有类型）&apos; ScreenPlay 得到的结果不满意时，您还可以使用免费开源的HandBrake!</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmInit.qml" line="74"/>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>文件类型无效。必须是 VP8 / VP9（*.webm）！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmInit.qml" line="97"/>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>将一个webm文件拖到这里，或者使用下面的&apos;选择文件&apos;</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmInit.qml" line="140"/>
        <source>Open Documentation</source>
        <translation>打开文档</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/ImportWebm/ImportWebmInit.qml" line="159"/>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="43"/>
        <source>AnalyseVideo...</source>
        <translation>分析视频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="46"/>
        <source>Generating preview image...</source>
        <translation>生成预览图...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="49"/>
        <source>Generating preview thumbnail image...</source>
        <translation>生成预览缩略图...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="56"/>
        <source>Generating 5 second preview video...</source>
        <translation>生成5秒预览视频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="59"/>
        <source>Generating preview gif...</source>
        <translation>生成预览GIF...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="68"/>
        <source>Converting Audio...</source>
        <translation>转换音频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="71"/>
        <source>Converting Video... This can take some time!</source>
        <translation>转换视频... 这可能需要一些时间！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="74"/>
        <source>Converting Video ERROR!</source>
        <translation>转换视频出错！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="77"/>
        <source>Analyse Video ERROR!</source>
        <translation>分析视频出错！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="106"/>
        <source>Import a video to a wallpaper</source>
        <translation>将视频导入为壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="215"/>
        <source>Generating preview video...</source>
        <translation>生成预览视频...</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="275"/>
        <source>Name (required!)</source>
        <translation>名称（必选）</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="289"/>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="297"/>
        <source>Youtube URL</source>
        <translation>Youtube 链接</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="328"/>
        <source>Abort</source>
        <translation>中止</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="341"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Convert.qml" line="375"/>
        <source>Save Wallpaper...</source>
        <translation>保存壁纸...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Init.qml" line="32"/>
        <source>Import a .mp4 video</source>
        <translation>导入.mp4视频</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Init.qml" line="48"/>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation>ScreenPlay V0.15以上可播放.mp4(通常称为h264)。这将提升在旧的系统上的性能.</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Init.qml" line="76"/>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation>无效文件类型，必须为合法的h264（.mp4）！</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Init.qml" line="100"/>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation>拖入.mp4文件到此处，或者使用下方的“选择文件”</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Init.qml" line="121"/>
        <source>Open Documentation</source>
        <translation>打开文档</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/Importh264/Importh264Init.qml" line="140"/>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="161"/>
        <source>Refreshing!</source>
        <translation>刷新中！</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="164"/>
        <location filename="../qml/Installed/Installed.qml" line="179"/>
        <source>Pull to refresh!</source>
        <translation>下拉以刷新！</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="209"/>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>从创意工坊获取更多壁纸和物件！</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="274"/>
        <source>Open containing folder</source>
        <translation>打开文件夹</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="284"/>
        <source>Remove Item</source>
        <translation>删除物品</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="284"/>
        <source>Remove via Workshop</source>
        <translation>从创意工坊中删除</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="294"/>
        <source>Open Workshop Page</source>
        <translation>打开创意工坊页面</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Installed.qml" line="306"/>
        <source>Are you sure you want to delete this item?</source>
        <translation>您确定要删除此物品？</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="78"/>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>从创意工坊免费获取物件和壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Installed/InstalledWelcomeScreen.qml" line="114"/>
        <source>Browse the Steam Workshop</source>
        <translation>浏览创意工坊</translation>
    </message>
</context>
<context>
    <name>LicenseSelector</name>
    <message>
        <location filename="../qml/Common/LicenseSelector.qml" line="17"/>
        <source>License</source>
        <translation>许可证</translation>
    </message>
    <message>
        <location filename="../qml/Common/LicenseSelector.qml" line="36"/>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material for any purpose, even commercially.</source>
        <translation>分享 — 复制与重分发这个素材，以任何媒介或格式。改动 — 为了任何目的，甚至是商业目的，对材料进行改编、改造和发展。</translation>
    </message>
    <message>
        <location filename="../qml/Common/LicenseSelector.qml" line="43"/>
        <location filename="../qml/Common/LicenseSelector.qml" line="64"/>
        <source>You grant other to remix your work and change the license to their liking.</source>
        <translation>您允许他人重制您的作品及改变许可证</translation>
    </message>
    <message>
        <location filename="../qml/Common/LicenseSelector.qml" line="50"/>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material. You are not allowed to use it commercially! </source>
        <translation>分享 — 复制与重分发这个素材，以任何媒介或格式。改动 — 对材料进行改编、改造和发展。不允许商业使用！</translation>
    </message>
    <message>
        <location filename="../qml/Common/LicenseSelector.qml" line="57"/>
        <source>You allow everyone to do anything with your work.</source>
        <translation>您允许所有人对您的作品做任何事。</translation>
    </message>
    <message>
        <location filename="../qml/Common/LicenseSelector.qml" line="71"/>
        <source>You grant other to remix your work but it must remain under the GPLv3. We recommend this license for all code wallpaper!</source>
        <translation>您授权他人对您的作品进行改编，但必须保持在GPLv3下发行。我们建议所有的代码壁纸都使用这个许可证!</translation>
    </message>
    <message>
        <location filename="../qml/Common/LicenseSelector.qml" line="78"/>
        <source>You do not share any rights and nobody is allowed to use or remix it (Not recommended). Can also used to credit work others.</source>
        <translation>您不分享任何权限，没有人可以使用或改编它（不推荐）。这也可用于计入他人的作品。</translation>
    </message>
</context>
<context>
    <name>MonitorConfiguration</name>
    <message>
        <location filename="../qml/Common/Dialogs/MonitorConfiguration.qml" line="38"/>
        <source>Your monitor setup changed!
 Please configure your wallpaper again.</source>
        <translation>您的显示器设置已更改！
 请重新配置您的壁纸。</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="60"/>
        <source>Wallpaper Configuration</source>
        <translation>壁纸配置</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="130"/>
        <source>Remove selected</source>
        <translation>移除已选择</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="145"/>
        <location filename="../qml/Monitors/Monitors.qml" line="161"/>
        <source>Remove </source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="145"/>
        <source>Wallpapers</source>
        <translation>壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/Monitors.qml" line="161"/>
        <source>Widgets</source>
        <translation>物件</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="143"/>
        <source>Set color</source>
        <translation>设置颜色</translation>
    </message>
    <message>
        <location filename="../qml/Monitors/MonitorsProjectSettingItem.qml" line="174"/>
        <source>Please choose a color</source>
        <translation>请选择颜色</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="60"/>
        <source>All</source>
        <translation>全部</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="79"/>
        <source>Scenes</source>
        <translation>场景</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="98"/>
        <source>Videos</source>
        <translation>视频</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="117"/>
        <source>Widgets</source>
        <translation>物件</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="163"/>
        <source>Install Date Ascending</source>
        <translation>安装日期↓</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Navigation.qml" line="163"/>
        <source>Install Date Descending</source>
        <translation>安装日期↑</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="41"/>
        <source> Subscribed items: </source>
        <translation>已订阅：</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Navigation.qml" line="89"/>
        <source>Upload to the Steam Workshop</source>
        <translation>上传到创意工坊</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/Navigation.qml" line="81"/>
        <source>Create</source>
        <translation>创建</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/Navigation.qml" line="92"/>
        <source>Workshop</source>
        <translation>创意工坊</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/Navigation.qml" line="103"/>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/Navigation.qml" line="115"/>
        <source>Community</source>
        <translation>社区</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/Navigation.qml" line="126"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="68"/>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation>设置活动壁纸或物件</translation>
    </message>
    <message>
        <location filename="../qml/Navigation/NavigationWallpaperConfiguration.qml" line="68"/>
        <source>No active Wallpaper or Widgets</source>
        <translation>没有活动壁纸或物件</translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="28"/>
        <source>You need to run Steam for this. steamErrorRestart: %1 - steamErrorAPIInit: %2</source>
        <translation>此功能需要启动Steam。 steamErrorRestart: %1 - steamErrorAPIInit: %2</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/PopupOffline.qml" line="33"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="25"/>
        <source>You Need to Agree To The Steam Subscriber Agreement First</source>
        <translation>您应先同意Steam订阅者协议</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="32"/>
        <source>REQUIRES INTERNET CONNECTION AND FREE STEAM ACCOUNT TO ACTIVATE. Notice: Product offered subject to your acceptance of the Steam Subscriber Agreement (SSA). You must activate this product via the Internet by registering for a Steam account and accepting the SSA. Please see https://store.steampowered.com/subscriber_agreement/ to view the SSA prior to purchase. If you do not agree with the provisions of the SSA, you should return this game unopened to your retailer in accordance with their return policy.</source>
        <translation>需要网络连接与免费Steam帐号以激活。注意：产品提供的前提是您接受Steam用户协议（SSA）。您必须通过网络注册一个Steam账户并接受SSA来激活该产品。购买前请打开https://store.steampowered.com/subscriber_agreement/ 查看SSA。如果您不同意SSA的规定，您应该根据零售商的退货政策将本游戏原封不动地退回。</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="49"/>
        <source>View The Steam Subscriber Agreement</source>
        <translation>查看Steam订阅者协议</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/PopupSteamWorkshopAgreement.qml" line="56"/>
        <source>Accept Steam Workshop Agreement</source>
        <translation>接受Steam创意工坊协议</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <location filename="../qml/Create/Wizards/QMLWallpaper.qml" line="29"/>
        <source>Create a QML Wallpaper</source>
        <translation>创建一个QML壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWallpaper.qml" line="34"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWallpaper.qml" line="44"/>
        <source>Wallpaper name</source>
        <translation>壁纸名</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWallpaper.qml" line="53"/>
        <source>Created By</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWallpaper.qml" line="62"/>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWallpaper.qml" line="70"/>
        <source>License &amp; Tags</source>
        <translation>许可证 &amp; 标签</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWallpaper.qml" line="93"/>
        <source>Preview Image</source>
        <translation>预览图</translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <location filename="../qml/Create/Wizards/QMLWidget.qml" line="20"/>
        <source>Create a QML widget</source>
        <translation>创建一个QML部件</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWidget.qml" line="74"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWidget.qml" line="82"/>
        <source>Widget name</source>
        <translation>部件名</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWidget.qml" line="90"/>
        <source>Created by</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/QMLWidget.qml" line="98"/>
        <source>Tags</source>
        <translation>标签</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <location filename="../qml/Monitors/SaveNotification.qml" line="39"/>
        <source>Profile saved successfully!</source>
        <translation>配置保存成功！</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <location filename="../qml/Installed/ScreenPlayItem.qml" line="198"/>
        <source>NEW</source>
        <translation>新</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/Common/Search.qml" line="33"/>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation>搜索壁纸和物件...</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="54"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="72"/>
        <source>Autostart</source>
        <translation>自启动</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="73"/>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay将在操作系统启动时启动，并会设置您的桌面。</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="84"/>
        <source>High priority Autostart</source>
        <translation>高优先级自启动</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="86"/>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>这个选项赋予ScreenPlay比其他应用程序更高的自启动优先级。</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="98"/>
        <source>Send anonymous crash reports and statistics</source>
        <translation>发送匿名崩溃报告和统计数据</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="99"/>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>帮助我们让 ScreenPlay 更快更稳定。所有被收集的数据完全匿名，而且仅用于开发用途！我们使用&lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; 收集与分析数据。&lt;b&gt;感谢他们&lt;/b&gt; 为我们提供对开源项目免费而优质的服务！</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="110"/>
        <source>Set save location</source>
        <translation>设置保存位置</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="111"/>
        <source>Set location</source>
        <translation>选择位置</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="116"/>
        <source>Your storage path is empty!</source>
        <translation>您的存储路径是空的！</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="137"/>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>注意：修改此目录并不影响创意工坊的下载路径。ScreenPlay仅支持单个内容文件夹！</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="159"/>
        <source>Language</source>
        <translation>语言</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="160"/>
        <source>Set the ScreenPlay UI Language</source>
        <translation>设置ScreenPlay界面语言</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="217"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="218"/>
        <source>Switch dark/light theme</source>
        <translation>切换到暗/亮主题</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="226"/>
        <source>System Default</source>
        <translation>跟随系统</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="229"/>
        <source>Dark</source>
        <translation>暗</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="232"/>
        <source>Light</source>
        <translation>亮</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="250"/>
        <source>Performance</source>
        <translation>性能</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="269"/>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>当其他应用程序在前台时，暂停壁纸视频渲染</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="270"/>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>我们禁用视频渲染（不是音频）以获得最佳性能。如果您有问题，可以在此处禁用此行为。 需要重启壁纸！</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="283"/>
        <source>Default Fill Mode</source>
        <translation>默认填充模式</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="284"/>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>设置此属性可定义视频的缩放方式以适应目标区域。</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="293"/>
        <source>Stretch</source>
        <translation>拉伸</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="296"/>
        <source>Fill</source>
        <translation>填充</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="299"/>
        <source>Contain</source>
        <translation>适应</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="302"/>
        <source>Cover</source>
        <translation>平铺</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="305"/>
        <source>Scale-Down</source>
        <translation>裁剪</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="320"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="352"/>
        <source>Thank you for using ScreenPlay</source>
        <translation>感谢您的使用</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="370"/>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>您好，我是Elias Steurer，也叫Kelteseth，我是ScreenPlay的开发者。感谢您使用我的软件。您可以在这里关注我，接收ScreenPlay的更新。</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="480"/>
        <source>Version</source>
        <translation>版本</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="481"/>
        <source>ScreenPlay Build Version 
</source>
<translation>ScreenPlay编译版本
</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="482"/>
        <source>Open Changelog</source>
        <translation>打开更改日志。</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="490"/>
        <source>Third Party Software</source>
        <translation>第三方软件</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="491"/>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay离不开一些人的帮助。非常感谢你们：</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="492"/>
        <source>Licenses</source>
        <translation>许可证</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="516"/>
        <source>Logs</source>
        <translation>日志</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="517"/>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>如果您的ScreenPlay出错，这是个很好的查错方式。它显示所有的日志和运行时警告。</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="518"/>
        <source>Show Logs</source>
        <translation>显示日志</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="534"/>
        <source>Data Protection</source>
        <translation>数据保护</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="535"/>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>我们使用您的数据提升ScreenPlay的体验。我们承诺不出售或分享这些匿名信息！</translation>
    </message>
    <message>
        <location filename="../qml/Settings/Settings.qml" line="536"/>
        <source>Privacy</source>
        <translation>隐私</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <location filename="../qml/Settings/SettingsExpander.qml" line="64"/>
        <source>Copy text to clipboard</source>
        <translation>复制文本至剪贴板</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="56"/>
        <source>Tools Overview</source>
        <translation>工具概览</translation>
    </message>
    <message>
        <source>Video import and convert (all types)</source>
        <translation type="vanished">视频导入及转换（任意类型）</translation>
    </message>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="63"/>
        <source>Video Import h264 (.mp4)</source>
        <translation>视频导入 h264(.mp4)</translation>
    </message>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="70"/>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation>视频导入 VP8 &amp; VP9 (.webm)</translation>
    </message>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="77"/>
        <source>Video import (all types)</source>
        <translation>视频导入 (所有类型)</translation>
    </message>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="84"/>
        <source>GIF Wallpaper</source>
        <translation>GIF 壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="91"/>
        <source>QML Wallpaper</source>
        <translation>QML 壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="98"/>
        <source>HTML5 Wallpaper</source>
        <translation>HTML5 壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="105"/>
        <source>Website Wallpaper</source>
        <translation>网页壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="112"/>
        <source>QML Widget</source>
        <translation>QML 部件</translation>
    </message>
    <message>
        <location filename="../qml/Create/Sidebar.qml" line="119"/>
        <source>HTML Widget</source>
        <translation>HTML 部件</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="74"/>
        <source>Set Wallpaper</source>
        <translation>设置壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="77"/>
        <source>Set Widget</source>
        <translation>设置物件</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="210"/>
        <source>Headline</source>
        <translation>标题</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="268"/>
        <source>Select a Monitor to display the content</source>
        <translation>选择显示此内容的显示器</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="299"/>
        <source>Set Volume</source>
        <translation>设置音量</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="318"/>
        <source>Fill Mode</source>
        <translation>填充模式</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="337"/>
        <source>Stretch</source>
        <translation>拉伸</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="340"/>
        <source>Fill</source>
        <translation>填充</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="343"/>
        <source>Contain</source>
        <translation>适应</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="346"/>
        <source>Cover</source>
        <translation>平铺</translation>
    </message>
    <message>
        <location filename="../qml/Installed/Sidebar.qml" line="349"/>
        <source>Scale-Down</source>
        <translation>裁剪</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="81"/>
        <source>Size: </source>
        <translation>大小：</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="87"/>
        <source>No description...</source>
        <translation>没有简介...</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="243"/>
        <source>Click here if you like the content</source>
        <translation>如果您喜欢它，点这里！</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="258"/>
        <source>Click here if you do not like the content</source>
        <translation>如果您不喜欢它，点这里</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="323"/>
        <source>Subscribtions: </source>
        <translation>订阅：</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="386"/>
        <source>Open In Steam</source>
        <translation>在Steam打开</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="397"/>
        <source>Subscribed!</source>
        <translation>已订阅！</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/Sidebar.qml" line="397"/>
        <source>Subscribe</source>
        <translation>订阅</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <location filename="../qml/Create/StartInfo.qml" line="19"/>
        <source>Free tools to help you to create wallpaper</source>
        <translation>免费的壁纸创建工具</translation>
    </message>
    <message>
        <location filename="../qml/Create/StartInfo.qml" line="36"/>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation>在下面有一些创建壁纸的工具，功能比ScreenPlay所提供的更为强大！</translation>
    </message>
</context>
<context>
    <name>SteamNotAvailable</name>
    <message>
        <location filename="../qml/Common/Dialogs/SteamNotAvailable.qml" line="11"/>
        <source>Could not load steam integration!</source>
        <translation>无法加载steam集成！</translation>
    </message>
</context>
<context>
    <name>SteamProfile</name>
    <message>
        <location filename="../qml/Workshop/SteamProfile.qml" line="77"/>
        <location filename="../qml/Workshop/SteamProfile.qml" line="137"/>
        <source>Back</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamProfile.qml" line="159"/>
        <source>Forward</source>
        <translation>前进</translation>
    </message>
</context>
<context>
    <name>SteamWorkshopStartPage</name>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="139"/>
        <source>Loading</source>
        <translation>正在加载</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="151"/>
        <source>Download now!</source>
        <translation>开始下载</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="156"/>
        <source>Downloading...</source>
        <translation>下载中...</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="164"/>
        <source>Details</source>
        <translation>查看详情</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="190"/>
        <source>Open In Steam</source>
        <translation>Steam中打开</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="274"/>
        <source>Profile</source>
        <translation>配置</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="293"/>
        <source>Upload</source>
        <translation>上传</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="317"/>
        <source>Search for Wallpaper and Widgets...</source>
        <translation>搜索壁纸和物件...</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="361"/>
        <source>Open Workshop in Steam</source>
        <translation>在Steam中打开创意工坊</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="385"/>
        <source>Ranked By Vote</source>
        <translation>评分最好</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="388"/>
        <source>Publication Date</source>
        <translation>发布日期</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="391"/>
        <source>Ranked By Trend</source>
        <translation>评分趋势</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="394"/>
        <source>Favorited By Friends</source>
        <translation>好友收藏</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="397"/>
        <source>Created By Friends</source>
        <translation>好友创建</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="400"/>
        <source>Created By Followed Users</source>
        <translation>已关注的</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="403"/>
        <source>Not Yet Rated</source>
        <translation>尚无评分</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="406"/>
        <source>Total VotesAsc</source>
        <translation>按总票数升序</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="409"/>
        <source>Votes Up</source>
        <translation>评分上升</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="412"/>
        <source>Total Unique Subscriptions</source>
        <translation>订阅总数</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="462"/>
        <source>Back</source>
        <translation>后退</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/SteamWorkshopStartPage.qml" line="486"/>
        <source>Forward</source>
        <translation>前进</translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="22"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="25"/>
        <source>Add tag</source>
        <translation>添加标签</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="129"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qml/Common/TagSelector.qml" line="151"/>
        <source>Add Tag</source>
        <translation>添加标签</translation>
    </message>
</context>
<context>
    <name>TextField</name>
    <message>
        <location filename="../qml/Common/TextField.qml" line="36"/>
        <source>Label</source>
        <translation>标识</translation>
    </message>
    <message>
        <location filename="../qml/Common/TextField.qml" line="98"/>
        <source>*Required</source>
        <translation>*必要的</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="10"/>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation>ScreenPlay - 双击以改变设置</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="29"/>
        <source>Open ScreenPlay</source>
        <translation>打开ScreenPlay</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="40"/>
        <location filename="../qml/Common/TrayIcon.qml" line="44"/>
        <source>Mute all</source>
        <translation>关闭全部提醒</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="48"/>
        <source>Unmute all</source>
        <translation>开启全部提醒</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="59"/>
        <location filename="../qml/Common/TrayIcon.qml" line="63"/>
        <source>Pause all</source>
        <translation>暂停全部</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="67"/>
        <source>Play all</source>
        <translation>播放全部</translation>
    </message>
    <message>
        <location filename="../qml/Common/TrayIcon.qml" line="74"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="31"/>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation>上传 壁纸/物件 到Steam</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="113"/>
        <source>Abort</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="128"/>
        <source>Upload Selected Projects</source>
        <translation>上传所选项目</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProject.qml" line="187"/>
        <source>Finish</source>
        <translation>结束</translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="114"/>
        <source>Type: </source>
        <translation>类型：</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="122"/>
        <source>Open Folder</source>
        <translation>打开文件夹</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectBigItem.qml" line="136"/>
        <source>Invalid Project!</source>
        <translation>无效的项目！</translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="35"/>
        <source>Fail</source>
        <translation>失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="38"/>
        <source>No Connection</source>
        <translation>没有连接</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="41"/>
        <source>Invalid Password</source>
        <translation>密码错误</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="44"/>
        <source>Logged In Elsewhere</source>
        <translation>在其他地方登录了</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="47"/>
        <source>Invalid Protocol Version</source>
        <translation>无效协议版本</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="50"/>
        <source>Invalid Param</source>
        <translation>参数无效</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="53"/>
        <source>File Not Found</source>
        <translation>文件未找到</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="56"/>
        <source>Busy</source>
        <translation>繁忙</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="59"/>
        <source>Invalid State</source>
        <translation>状态无效</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="62"/>
        <source>Invalid Name</source>
        <translation>名称无效</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="65"/>
        <source>Invalid Email</source>
        <translation>邮箱无效</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="68"/>
        <source>Duplicate Name</source>
        <translation>重复名称</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="71"/>
        <source>Access Denied</source>
        <translation>拒绝访问</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="74"/>
        <source>Timeout</source>
        <translation>超时</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="77"/>
        <source>Banned</source>
        <translation>被禁止</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="80"/>
        <source>Account Not Found</source>
        <translation>账户未找到</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="83"/>
        <source>Invalid SteamID</source>
        <translation>SteamID无效</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="86"/>
        <source>Service Unavailable</source>
        <translation>服务不可用</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="89"/>
        <source>Not Logged On</source>
        <translation>未登录</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="92"/>
        <source>Pending</source>
        <translation>待处理</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="95"/>
        <source>Encryption Failure</source>
        <translation>加密失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="98"/>
        <source>Insufficient Privilege</source>
        <translation>权限不足</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="101"/>
        <source>Limit Exceeded</source>
        <translation>超出限制</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="104"/>
        <source>Revoked</source>
        <translation>被撤回</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="107"/>
        <source>Expired</source>
        <translation>已过期</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="110"/>
        <source>Already Redeemed</source>
        <translation>已兑换</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="113"/>
        <source>Duplicate Request</source>
        <translation>重复请求</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="116"/>
        <source>Already Owned</source>
        <translation>已拥有</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="119"/>
        <source>IP Not Found</source>
        <translation>未找到IP</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="122"/>
        <source>Persist Failed</source>
        <translation>持续失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="125"/>
        <source>Locking Failed</source>
        <translation>锁定失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="128"/>
        <source>Logon Session Replaced</source>
        <translation>登录会话被覆盖</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="131"/>
        <source>Connect Failed</source>
        <translation>连接失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="134"/>
        <source>Handshake Failed</source>
        <translation>握手失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="137"/>
        <source>IO Failure</source>
        <translation>输入输出失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="140"/>
        <source>Remote Disconnect</source>
        <translation>远程断开连接</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="143"/>
        <source>Shopping Cart Not Found</source>
        <translation>未找到购物车</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="146"/>
        <source>Blocked</source>
        <translation>受阻</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="149"/>
        <source>Ignored</source>
        <translation>已忽略</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="152"/>
        <source>No Match</source>
        <translation>无匹配</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="155"/>
        <source>Account Disabled</source>
        <translation>账号已禁用</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="158"/>
        <source>Service ReadOnly</source>
        <translation>服务只读</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="161"/>
        <source>Account Not Featured</source>
        <translation>账号未显示</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="164"/>
        <source>Administrator OK</source>
        <translation>管理员确定</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="167"/>
        <source>Content Version</source>
        <translation>内容版本</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="170"/>
        <source>Try Another CM</source>
        <translation>尝试其他内容管理器</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="173"/>
        <source>Password Required To Kick Session</source>
        <translation>需要密码以启动会话</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="176"/>
        <source>Already Logged In Elsewhere</source>
        <translation>已在其他地方登录</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="179"/>
        <source>Suspended</source>
        <translation>已挂起</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="182"/>
        <source>Cancelled</source>
        <translation>已取消</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="185"/>
        <source>Data Corruption</source>
        <translation>数据损坏</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="188"/>
        <source>Disk Full</source>
        <translation>磁盘满</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="191"/>
        <source>Remote Call Failed</source>
        <translation>远程调用失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="194"/>
        <source>Password Unset</source>
        <translation>解除密码</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="197"/>
        <source>External Account Unlinked</source>
        <translation>外部账户未链接</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="200"/>
        <source>PSN Ticket Invalid</source>
        <translation>PSN令牌无效</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="203"/>
        <source>External Account Already Linked</source>
        <translation>外部账户已链接</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="206"/>
        <source>Remote File Conflict</source>
        <translation>远程文件冲突</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="209"/>
        <source>Illegal Password</source>
        <translation>非法密码</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="212"/>
        <source>Same As Previous Value</source>
        <translation>与旧值相同</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="215"/>
        <source>Account Logon Denied</source>
        <translation>帐户登录被拒绝</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="218"/>
        <source>Cannot Use Old Password</source>
        <translation>不可使用旧密码</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="221"/>
        <source>Invalid Login AuthCode</source>
        <translation>无效的登录授权码</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="224"/>
        <source>Account Logon Denied No Mail</source>
        <translation>帐户登录被拒绝：没有邮件</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="227"/>
        <source>Hardware Not Capable Of IPT</source>
        <translation>硬件不支持身份保护技术</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="230"/>
        <source>IPT Init Error</source>
        <translation>身份保护技术初始化失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="233"/>
        <source>Parental Control Restricted</source>
        <translation>家长控制限制</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="236"/>
        <source>Facebook Query Error</source>
        <translation>Facebook查询错误</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="239"/>
        <source>Expired Login Auth Code</source>
        <translation>过期的登录授权码</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="242"/>
        <source>IP Login Restriction Failed</source>
        <translation>IP登录限制失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="245"/>
        <source>Account Locked Down</source>
        <translation>帐户被锁定</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="248"/>
        <source>Account Logon Denied Verified Email Required</source>
        <translation>帐户登录被拒绝：需验证电子邮件</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="251"/>
        <source>No MatchingURL</source>
        <translation>没有匹配网址</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="254"/>
        <source>Bad Response</source>
        <translation>坏响应</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="257"/>
        <source>Require Password ReEntry</source>
        <translation>要求重新输入密码</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="260"/>
        <source>Value Out Of Range</source>
        <translation>值超出范围</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="263"/>
        <source>Unexpecte Error</source>
        <translation>意外错误</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="266"/>
        <source>Disabled</source>
        <translation>已禁用</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="269"/>
        <source>Invalid CEG Submission</source>
        <translation>无效的CEG提交</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="272"/>
        <source>Restricted Device</source>
        <translation>受限设备</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="275"/>
        <source>Region Locked</source>
        <translation>地区锁定</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="278"/>
        <source>Rate Limit Exceeded</source>
        <translation>超出比率限制</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="281"/>
        <source>Account Login Denied Need Two Factor</source>
        <translation>账户登录被拒绝：需要两步验证</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="284"/>
        <source>Item Deleted</source>
        <translation>物品已删除</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="287"/>
        <source>Account Login Denied Throttle</source>
        <translation>帐户登录被拒绝</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="290"/>
        <source>Two Factor Code Mismatch</source>
        <translation>两步验证码不匹配</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="293"/>
        <source>Two Factor Activation Code Mismatch</source>
        <translation>两步验证激活码不匹配</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="296"/>
        <source>Account Associated To Multiple Partners</source>
        <translation>帐户已关联到多个合作伙伴</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="299"/>
        <source>Not Modified</source>
        <translation>未修改</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="302"/>
        <source>No Mobile Device</source>
        <translation>没有移动设备</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="305"/>
        <source>Time Not Synced</source>
        <translation>时间未同步</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="308"/>
        <source>Sms Code Failed</source>
        <translation>短信验证码失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="311"/>
        <source>Account Limit Exceeded</source>
        <translation>超出账户限制</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="314"/>
        <source>Account Activity Limit Exceeded</source>
        <translation>超出账户活动限制</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="317"/>
        <source>Phone Activity Limit Exceeded</source>
        <translation>超出电话活动限制</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="320"/>
        <source>Refund To Wallet</source>
        <translation>退款到钱包</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="323"/>
        <source>Email Send Failure</source>
        <translation>邮件发送失败</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="326"/>
        <source>Not Settled</source>
        <translation>未解决</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="329"/>
        <source>Need Captcha</source>
        <translation>需要验证</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="332"/>
        <source>GSLT Denied</source>
        <translation>服务器令牌拒绝</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="335"/>
        <source>GS Owner Denied</source>
        <translation>服务器所有者拒绝</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="338"/>
        <source>Invalid Item Type</source>
        <translation>无效的物品类型</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="341"/>
        <source>IP Banned</source>
        <translation>IP被禁用</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="344"/>
        <source>GSLT Expired</source>
        <translation>服务器令牌过期</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="347"/>
        <source>Insufficient Funds</source>
        <translation>资金不足</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="350"/>
        <source>Too Many Pending</source>
        <translation>提交中太多</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="353"/>
        <source>No Site Licenses Found</source>
        <translation>无网站证书</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="356"/>
        <source>WG Network Send Exceeded</source>
        <translation>蠕虫守护网络发送超限</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="359"/>
        <source>Account Not Friends</source>
        <translation>帐户不是好友</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="362"/>
        <source>Limited User Account</source>
        <translation>受限账户</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="365"/>
        <source>Cant Remove Item</source>
        <translation>无法移除物品</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="368"/>
        <source>Account Deleted</source>
        <translation>账户已删除</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="371"/>
        <source>Existing User Cancelled License</source>
        <translation>现有用户已取消许可证</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="374"/>
        <source>Community Cooldown</source>
        <translation>社区降温</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="466"/>
        <source>Status:</source>
        <translation>状态：</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/upload/UploadProjectItem.qml" line="485"/>
        <source>Upload Progress: </source>
        <translation>上传进度：</translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <location filename="../qml/Create/Wizards/WebsiteWallpaper.qml" line="30"/>
        <source>Create a Website Wallpaper</source>
        <translation>创建一个网站壁纸</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/WebsiteWallpaper.qml" line="35"/>
        <source>General</source>
        <translation>常规</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/WebsiteWallpaper.qml" line="45"/>
        <source>Wallpaper name</source>
        <translation>壁纸名</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/WebsiteWallpaper.qml" line="54"/>
        <source>Created By</source>
        <translation>作者</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/WebsiteWallpaper.qml" line="63"/>
        <source>Description</source>
        <translation>简介</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/WebsiteWallpaper.qml" line="79"/>
        <source>Tags</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/WebsiteWallpaper.qml" line="93"/>
        <source>Preview Image</source>
        <translation>预览图</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <location filename="../qml/Create/Wizards/WizardPage.qml" line="69"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../qml/Create/Wizards/WizardPage.qml" line="101"/>
        <source>Saving...</source>
        <translation>保存中...</translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="298"/>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation>成功订阅创意工坊物品！</translation>
    </message>
    <message>
        <location filename="../qml/Workshop/WorkshopItem.qml" line="401"/>
        <source>Download complete!</source>
        <translation>下载完成！</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <location filename="../qml/Community/XMLNewsfeed.qml" line="71"/>
        <source>News &amp; Patchnotes</source>
        <translation>新闻和更改日志</translation>
    </message>
</context>
</TS>
