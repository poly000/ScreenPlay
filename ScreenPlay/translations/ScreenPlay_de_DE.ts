<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>ColorPicker</name>
    <message>
        <source>Red</source>
        <translation>Rot</translation>
    </message>
    <message>
        <source>Green</source>
        <translation>Grün</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation>Blau</translation>
    </message>
    <message>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <source>HSV</source>
        <translation>HSV</translation>
    </message>
    <message>
        <source>R:</source>
        <translation>R:</translation>
    </message>
    <message>
        <source>G:</source>
        <translation>G:</translation>
    </message>
    <message>
        <source>B:</source>
        <translation>B:</translation>
    </message>
    <message>
        <source>H:</source>
        <translation>H:</translation>
    </message>
    <message>
        <source>S:</source>
        <translation>S:</translation>
    </message>
    <message>
        <source>V:</source>
        <translation>V:</translation>
    </message>
    <message>
        <source>Alpha:</source>
        <translation>Alpha</translation>
    </message>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
</context>
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Neuigkeiten</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message>
        <source>Issue List</source>
        <translation>Fehler Liste</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Beitragen</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Steam Workshop</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Öffne in</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Importiere Video jeden Typs</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>Je nach deiner PC Konfiguaration ist es besser dein Wallpaper mit einem Spezifischen Video Kodierer zu Konvertieren. Wenn allerdings beides schlecht läuft kannst du ein QML Wallpaper Probieren! Unterstützte Video-Formate sind:
*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Bevorzugte Video-Kodierung Festlegen</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Qualitäts-Regler. Niedriger wert heißt niedrige Qualität</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Öffne Documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Datei auswählen</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Es ist ein Fehler aufgetreten!</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Kopiere den Text in die Zwischenablage</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Zurück zum Erstellen und einen Fehlerbericht senden!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Erzeuge Vorschaubild...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Erzeuge Vorschau-Miniaturbild...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generiere ein 5-Sekunden-Vorschau-Video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generiere Vorschau-Gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Konvertiere Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Video wird umgewandelt... Das kann etwas dauern!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Konvertieren nicht erfolgreich!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Analyse des Videos schlug Fehl!</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Konvertiere ein Video in ein Hintergrund Live Wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generiere Vorschau-Video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Name (erforderlich!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>YouTube-URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Speicher Wallpaper...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Lautstärke</translation>
    </message>
    <message>
        <source>Playback rate</source>
        <translation>Wiedergabegeschwindigkeit</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Aktuelle Videozeit</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Füll-Modus</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Strecken</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Ausfüllen</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Enthält</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Cover</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>Runter Skallieren</translation>
    </message>
</context>
<context>
    <name>FileSelector</name>
    <message>
        <source>Clear</source>
        <translation>Leeren</translation>
    </message>
    <message>
        <source>Select File</source>
        <translation>Datei auswählen</translation>
    </message>
    <message>
        <source>Please choose a file</source>
        <translation>Bitte Wählen Sie eine Datei aus</translation>
    </message>
</context>
<context>
    <name>Forum</name>
    <message>
        <source>Download Wallpaper and Widgets from our forums manually. If you want to download Steam Workshop content you have to install ScreenPlay via Steam.</source>
        <translation>Du kannst Wallpaper und Widgets aus unserem Forum manuell herunterladen. Wenn du den Steam Workshop-Inhalt herunterladen möchtest, musst du ScreenPlay via Steam installieren.</translation>
    </message>
    <message>
        <source>Install Steam Version</source>
        <translation>Steam Version installieren</translation>
    </message>
    <message>
        <source>Open In Browser</source>
        <translation>Im Browser öffnen</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>Importiere ein GIF Wallpaper</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>Ziehe eine .gif Datei hierher und benutze &apos;Datei auswählen&apos; darunter.</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>Wähle dein GIF aus</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Wallpaper Name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Schlagwörter</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>Erstelle ein HTML Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Wallpaper Name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Lizenz &amp; Schlagwörter</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Vorschau Bild</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>Erstelle ein HTML Widget</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Widget Name</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Schlagwörter</translation>
    </message>
</context>
<context>
    <name>HeadlineSection</name>
    <message>
        <source>Headline Section</source>
        <translation>Überschrift auswahl</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <source>Set your own preview image</source>
        <translation>Setze dein eigenes Vorschaubild</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Leeren</translation>
    </message>
    <message>
        <source>Select Preview Image</source>
        <translation>Wähle ein Vorschaubild</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>Analysiere Video...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Erzeuge Vorschaubild...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Erzeuge Vorschau-Miniaturbild...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generiere ein 5-Sekunden-Vorschau-Video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generiere Vorschau-Gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Konvertiere Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Video wird umgewandelt... Das kann etwas dauern!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Konvertieren nicht erfolgreich!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Analyse des Videos schlug Fehl!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Importiere ein Video zu ein Wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generiere Vorschau-Video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Name (erforderlich!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>YouTube-URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Speicher Wallpaper...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>Importiere ein .webm Video</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>Wenn WEBM importiert wird, kann die lange Umwandlungszeit übersprungen werden. Wenn du nicht mit dem Ergebnis von dem ScreenPlay importierer zu frieden bis &apos; Video Importierer und Convertierer&apos; kannst du auch den gratis und Open Source konvertierer HandBreak benutzen!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>Ungültiger Dateityp. Es muss ein gültiger VP8 oder Vp9 (*.webm) typ sein!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>Lass hier eine *.webm Datei fallen oder benutze &apos;Datei auswählen&apos; darunter</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Öffne Dokumentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Datei auswählen</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished">Analysiere Video...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished">Erzeuge Vorschaubild...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Erzeuge Vorschau-Miniaturbild...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Generiere ein 5-Sekunden-Vorschau-Video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Generiere Vorschau-Gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished">Konvertiere Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Video wird umgewandelt... Das kann etwas dauern!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Konvertieren nicht erfolgreich!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished">Analyse des Videos schlug Fehl!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Importiere ein Video zu ein Wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished">Generiere Vorschau-Video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished">Name (erforderlich!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Beschreibung</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished">YouTube-URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished">Abbrechen</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Speichern</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Speicher Wallpaper...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished">Datei auswählen</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Aktualisiere!</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>Drücken zum aktualisieren!</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Holen dir mehr Wallpaper und Widgets über den Steam-Workshop!</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>Enthaltenden Ordner öffnen</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Item entfernen</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Über den Workshop entfernen</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Workshop öffnen</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Bist du dir sicher dass du dieses Item löschen möchtest?</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Hole dir kostenlose Widgets und Wallpaper via Steam</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Stöbere durch den Steam Workshop</translation>
    </message>
</context>
<context>
    <name>LicenseSelector</name>
    <message>
        <source>License</source>
        <translation>Lizenz</translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material for any purpose, even commercially.</source>
        <translation>Teilen - kopieren und teilen in jeglicher art. Anpassen - remixen, Transformieren, und gebaut auf dem Material das für jeden Zweck, auch kommerziell.</translation>
    </message>
    <message>
        <source>You grant other to remix your work and change the license to their liking.</source>
        <translation>Sie gestatten anderen, Ihr Werk neu zu mixen und die Lizenz auf deren Ermessen zu ändern.</translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material. You are not allowed to use it commercially! </source>
        <translation>Teilen — Kopieren und Weitergeben des Materials in jedem Medium oder Format. Geändert — Remix, transformiere und baue auf dem Material. Du darfst es nicht kommerziell verwenden! </translation>
    </message>
    <message>
        <source>You allow everyone to do anything with your work.</source>
        <translation>Du erlaubst allen, alles mit deiner Arbeit zu tun.</translation>
    </message>
    <message>
        <source>You grant other to remix your work but it must remain under the GPLv3. We recommend this license for all code wallpaper!</source>
        <translation>Du gewährst anderen das Recht auf Remix deiner Arbeit, aber es muss unter der GPLv3 Lizenz verbleiben. Wir empfehlen diese Lizenz für alle Code-Wallpaper!</translation>
    </message>
    <message>
        <source>You do not share any rights and nobody is allowed to use or remix it (Not recommended). Can also used to credit work others.</source>
        <translation>Du teilst keine Rechte und niemand darf sie benutzen oder neu verwenden (Nicht empfohlen). Es kann auch verwendet werden, um Werke andere zu kreditieren.</translation>
    </message>
</context>
<context>
    <name>MonitorConfiguration</name>
    <message>
        <source>Your monitor setup changed!
 Please configure your wallpaper again.</source>
        <translation>Deine Bildschirm aufstellung hat sich geändert!
Bitte Konfiguriere deine Wallpaper noch erneut</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>Wallpaper Konfiguration</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Die Auswahl entfernen</translation>
    </message>
    <message>
        <source>Remove </source>
        <translation>Entferne </translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>Hintergründe</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>Farbe Festlegen</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>Bitte wähle eine Farbe aus</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>All</source>
        <translation>Alles</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation>Szenen</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation>Videos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
    <message>
        <source>Install Date Ascending</source>
        <translation>Installationsdatum aufsteigend</translation>
    </message>
    <message>
        <source>Install Date Descending</source>
        <translation>Installationsdatum absteigend</translation>
    </message>
    <message>
        <source> Subscribed items: </source>
        <translation>Abonnierte Inhalte: </translation>
    </message>
    <message>
        <source>Upload to the Steam Workshop</source>
        <translation>Zum Steam Workshop Hochladen</translation>
    </message>
    <message>
        <source>Create</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation>Workshop</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>Installiert</translation>
    </message>
    <message>
        <source>Community</source>
        <translation>Community</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation>Konfiguriere aktive Wallpaper oder Widgets</translation>
    </message>
    <message>
        <source>No active Wallpaper or Widgets</source>
        <translation>Keine aktiven Wallpaper oder Widgets</translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <source>You need to run Steam for this. steamErrorRestart: %1 - steamErrorAPIInit: %2</source>
        <translation>Du musst Steam ausführen, um dies zu tun. SteamErrorRestart: %1 - steamErrorAPIInit: %2</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <source>You Need to Agree To The Steam Subscriber Agreement First</source>
        <translation>Du musst zuerst zu den Steam-Abonnentenvertrag zustimmen</translation>
    </message>
    <message>
        <source>REQUIRES INTERNET CONNECTION AND FREE STEAM ACCOUNT TO ACTIVATE. Notice: Product offered subject to your acceptance of the Steam Subscriber Agreement (SSA). You must activate this product via the Internet by registering for a Steam account and accepting the SSA. Please see https://store.steampowered.com/subscriber_agreement/ to view the SSA prior to purchase. If you do not agree with the provisions of the SSA, you should return this game unopened to your retailer in accordance with their return policy.</source>
        <translation type="unfinished">REQUIRES INTERNET CONNECTION AND FREE STEAM ACCOUNT TO ACTIVATE. Notice: Product offered subject to your acceptance of the Steam Subscriber Agreement (SSA). You must activate this product via the Internet by registering for a Steam account and accepting the SSA. Please see https://store.steampowered.com/subscriber_agreement/ to view the SSA prior to purchase. If you do not agree with the provisions of the SSA, you should return this game unopened to your retailer in accordance with their return policy.</translation>
    </message>
    <message>
        <source>View The Steam Subscriber Agreement</source>
        <translation>Siehe dir den Steam-Abonnentenvertrag</translation>
    </message>
    <message>
        <source>Accept Steam Workshop Agreement</source>
        <translation>Aktzeptiere den Steam-Abonnentenvertrag</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>Erstelle ein QML Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Wallpaper Name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Lizenz &amp; Schlagwörter</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Vorschaubild</translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>Erstelle ein QML Widget</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Widget Name</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Schlagwörter</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>Profil erfolgreich gespeichert!</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>NEU</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation>Suche nach Wallpaper &amp; Widgets</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay startet mit Windows und richtet deinen Desktop jedes Mal für dich ein.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>Hohe Priorität für Autostart</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>Diese Option gewährt ScreenPlay eine höhere Autostartpriorität als anderen Anwendungen.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation>Sende anonyme Absturzberichte und Statistiken</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>Helfen Sie uns, ScreenPlay schneller und stabiler zu machen. Alle gesammelten Daten sind rein anonym und werden nur für Entwicklungszwecke verwendet! Wir benutzen &lt;a href=&quot;https://sentry.io&quot;&gt;Sentry. o&lt;/a&gt; um diese Daten zu sammeln und zu analysieren. Ein &lt;b&gt;großes Dankeschön an sie&lt;/b&gt; für die kostenlose Premium-Unterstützung für Open Source Projekte!</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation>Speicherort auswählen</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation>Standort auswählen</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation>Dein Speicherpfad ist leer!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>Wichtig: Eine Änderung dieses Verzeichnisses hat keine Auswirkungen auf den Download-Pfad des Workshops. ScreenPlay unterstützt nur ein Verzeichnis!</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>Wähle die Sprache des Programms aus</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>Wechsle Dunkles/Helles Design</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>System Standard</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Hell</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Leistung</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>Pausiere Wallpaper Video Rendering wenn eine andere App im Vordergrund ist</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>Wir deaktivieren das Video Rendering (Aber nicht die Sounds) für die beste Leistung. Wenn du damit probleme haben solltest kannst dieses Verhalten hier ausschalten. Ein Neustart wird aber von Nöten sein!</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>Standard-Füllmodus</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>Lege diese Eigenschaft fest, um zu definieren, wie das Video skaliert wird, damit es in den Zielbereich passt.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Strecken</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Ausfüllen</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Enthält</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Cover</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Runter Skallieren</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>Danke, dass du ScreenPlay verwendest</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>Moin, ich bin Elias Steurer, auch bekannt als Kelteseth und ich bin der Entwickler von ScreenPlay. Danke, dass du meine Software nutzt. Du kannst mir hier folgen, um Updates über ScreenPlay zu erhalten:</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>ScreenPlay Build Version </source>
        <translation type="vanished">ScreenPlay-Build-Version </translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation>Changelog öffnen</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation>Software von Drittanbietern</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay wäre ohne die Arbeit anderer nicht möglich. Ein großes Dankeschön dafür geht an: </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation>Lizenzen</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation>Protokolle</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>Wenn den ScreenPlay sich falsch verhält, ist hier eine gute Möglichkeit, nach Antworten zu suchen. Hier werden alle Protokolle und Warnungen während der Laufzeit angezeigt.</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation>Zeige Logs</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation>Datenschutz</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>Wir verwenden deine Daten sehr sorgfältig, um ScreenPlay zu verbessern. Wir verkaufen oder teilen diese (anonymen) Informationen nicht mit anderen!</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>Datenschutz</translation>
    </message>
    <message>
        <source>ScreenPlay Build Version 
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Kopiere den Text in die Zwischenablage</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation>Werkzeugeübersicht</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation>GIF Wallpaper</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation>QML Wallpaper</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation>HTML5 Wallpaper</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation>Website Wallpaper</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation>QML Widget</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation>HTML Widget</translation>
    </message>
    <message>
        <source>Set Wallpaper</source>
        <translation>Wallpaper Festlegen</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation>Widget wählen</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation>Überschrift</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation>Wähle einen Monitor zur Anzeige des Inhalts</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation>Audiolautstärke einstellen</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Füll-Modus</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Strecken</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Ausfüllen</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Enthält</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Cover</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Runter-Skallieren</translation>
    </message>
    <message>
        <source>Size: </source>
        <translation>Größe: </translation>
    </message>
    <message>
        <source>No description...</source>
        <translation>Leine Beschreibung...</translation>
    </message>
    <message>
        <source>Click here if you like the content</source>
        <translation>Klicke hier wenn du den Inhalt magst</translation>
    </message>
    <message>
        <source>Click here if you do not like the content</source>
        <translation>Klicke hier wenn du den Inhalt nicht magst</translation>
    </message>
    <message>
        <source>Subscribtions: </source>
        <translation>Abonnements</translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation>Öffne in Steam</translation>
    </message>
    <message>
        <source>Subscribed!</source>
        <translation>Abonniert</translation>
    </message>
    <message>
        <source>Subscribe</source>
        <translation>Abonniere</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SteamNotAvailable</name>
    <message>
        <source>Could not load steam integration!</source>
        <translation>Konnte Steam integration nicht Laden!</translation>
    </message>
</context>
<context>
    <name>SteamProfile</name>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Vor</translation>
    </message>
</context>
<context>
    <name>SteamWorkshopStartPage</name>
    <message>
        <source>Loading</source>
        <translation type="unfinished">Loading</translation>
    </message>
    <message>
        <source>Download now!</source>
        <translation>Jetzt herunterladen!</translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation>Herunterladen...</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation>In Steam öffnen</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>Hochladen</translation>
    </message>
    <message>
        <source>Search for Wallpaper and Widgets...</source>
        <translation>Suche nach Wallpaper &amp; Widgets...</translation>
    </message>
    <message>
        <source>Open Workshop in Steam</source>
        <translation>Im Steam Workshop öffnen</translation>
    </message>
    <message>
        <source>Ranked By Vote</source>
        <translation>Nach Bewertungen sortieren</translation>
    </message>
    <message>
        <source>Publication Date</source>
        <translation>Nach Veröffentlichungs Datum sortieren</translation>
    </message>
    <message>
        <source>Ranked By Trend</source>
        <translation>Nach Trends sortieren</translation>
    </message>
    <message>
        <source>Favorited By Friends</source>
        <translation>Von Freunden Favorisiert</translation>
    </message>
    <message>
        <source>Created By Friends</source>
        <translation>Von Freunden erstellt</translation>
    </message>
    <message>
        <source>Created By Followed Users</source>
        <translation>Von gefolgten Benutzern erstellt</translation>
    </message>
    <message>
        <source>Not Yet Rated</source>
        <translation>Noch nicht Bewertet</translation>
    </message>
    <message>
        <source>Total VotesAsc</source>
        <translation>Abstimmungs Anzahl Absteigend</translation>
    </message>
    <message>
        <source>Votes Up</source>
        <translation>Abstimmungs Anzahl Steigend</translation>
    </message>
    <message>
        <source>Total Unique Subscriptions</source>
        <translation>Anzahl einzigartiger Abonnenten</translation>
    </message>
    <message>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Vor</translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Add tag</source>
        <translation>Tag hinzufügen</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Add Tag</source>
        <translation>Tag hinzufügen</translation>
    </message>
</context>
<context>
    <name>TextField</name>
    <message>
        <source>Label</source>
        <translation>Beschriftung</translation>
    </message>
    <message>
        <source>*Required</source>
        <translation>*Benötigt</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation>ScreenPlay - Doppelklicke, um deine Einstellungen zu ändern.</translation>
    </message>
    <message>
        <source>Open ScreenPlay</source>
        <translation>Öffne ScreenPlay</translation>
    </message>
    <message>
        <source>Mute all</source>
        <translation>Alles stummschalten</translation>
    </message>
    <message>
        <source>Unmute all</source>
        <translation>Alle Stummschaltungen aufheben</translation>
    </message>
    <message>
        <source>Pause all</source>
        <translation>Alles pausieren</translation>
    </message>
    <message>
        <source>Play all</source>
        <translation>Alles abspielen</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation>Lade Wallpaper/Widget auf Steam hoch</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Upload Selected Projects</source>
        <translation>Lade ausgewähltes Projekt hoch</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation>Beenden</translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <source>Type: </source>
        <translation>Typ: </translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation>Öffne Ordner</translation>
    </message>
    <message>
        <source>Invalid Project!</source>
        <translation>Ungültiges Projekt</translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <source>Fail</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <source>No Connection</source>
        <translation>Keine Verbindung</translation>
    </message>
    <message>
        <source>Invalid Password</source>
        <translation>Falsches Passwort</translation>
    </message>
    <message>
        <source>Logged In Elsewhere</source>
        <translation>Woanders Eingelogged</translation>
    </message>
    <message>
        <source>Invalid Protocol Version</source>
        <translation>Falsche Protokoll Version</translation>
    </message>
    <message>
        <source>Invalid Param</source>
        <translation>Falsche Parameter</translation>
    </message>
    <message>
        <source>File Not Found</source>
        <translation>Datei nicht gefunden</translation>
    </message>
    <message>
        <source>Busy</source>
        <translation>Beschäftigt</translation>
    </message>
    <message>
        <source>Invalid State</source>
        <translation>ungültiger Status</translation>
    </message>
    <message>
        <source>Invalid Name</source>
        <translation>ungültiger Name</translation>
    </message>
    <message>
        <source>Invalid Email</source>
        <translation>ungültige Email</translation>
    </message>
    <message>
        <source>Duplicate Name</source>
        <translation>Doppelter Name</translation>
    </message>
    <message>
        <source>Access Denied</source>
        <translation>Zugriff verweigert</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation>Auszeit</translation>
    </message>
    <message>
        <source>Banned</source>
        <translation>Gebannt</translation>
    </message>
    <message>
        <source>Account Not Found</source>
        <translation>Account nicht gefunden</translation>
    </message>
    <message>
        <source>Invalid SteamID</source>
        <translation>ungültige Steam ID</translation>
    </message>
    <message>
        <source>Service Unavailable</source>
        <translation>Service nicht Verfügbar</translation>
    </message>
    <message>
        <source>Not Logged On</source>
        <translation>Nicht angemeldet</translation>
    </message>
    <message>
        <source>Pending</source>
        <translation>Ausstehend</translation>
    </message>
    <message>
        <source>Encryption Failure</source>
        <translation>Verschlüsselungsfehler</translation>
    </message>
    <message>
        <source>Insufficient Privilege</source>
        <translation>Unzureichende Berechtigung</translation>
    </message>
    <message>
        <source>Limit Exceeded</source>
        <translation>Limit überschritten</translation>
    </message>
    <message>
        <source>Revoked</source>
        <translation>Wiederrufen</translation>
    </message>
    <message>
        <source>Expired</source>
        <translation>Abgelaufen</translation>
    </message>
    <message>
        <source>Already Redeemed</source>
        <translation>Bereits eingelöst</translation>
    </message>
    <message>
        <source>Duplicate Request</source>
        <translation>Doppelte anfrage</translation>
    </message>
    <message>
        <source>Already Owned</source>
        <translation>Bereits vergeben</translation>
    </message>
    <message>
        <source>IP Not Found</source>
        <translation>IP nicht gefunden</translation>
    </message>
    <message>
        <source>Persist Failed</source>
        <translation>Anhalten fehlgeschlagen</translation>
    </message>
    <message>
        <source>Locking Failed</source>
        <translation>Sperren fehlgeschlagen</translation>
    </message>
    <message>
        <source>Logon Session Replaced</source>
        <translation>Anmeldesitzung ersetzt</translation>
    </message>
    <message>
        <source>Connect Failed</source>
        <translation>Verbindung gescheitert</translation>
    </message>
    <message>
        <source>Handshake Failed</source>
        <translation>Handshake fehlgeschlagen</translation>
    </message>
    <message>
        <source>IO Failure</source>
        <translation>IO Fehler</translation>
    </message>
    <message>
        <source>Remote Disconnect</source>
        <translation type="unfinished">Remote Disconnect</translation>
    </message>
    <message>
        <source>Shopping Cart Not Found</source>
        <translation>Warenkorb nicht gefunden</translation>
    </message>
    <message>
        <source>Blocked</source>
        <translation>Blockiert</translation>
    </message>
    <message>
        <source>Ignored</source>
        <translation>Ignoriert</translation>
    </message>
    <message>
        <source>No Match</source>
        <translation>Keine Übereinstimmung</translation>
    </message>
    <message>
        <source>Account Disabled</source>
        <translation>Account deaktiviert</translation>
    </message>
    <message>
        <source>Service ReadOnly</source>
        <translation>Schreibgeschützter Dienst</translation>
    </message>
    <message>
        <source>Account Not Featured</source>
        <translation type="unfinished">Account Not Featured</translation>
    </message>
    <message>
        <source>Administrator OK</source>
        <translation type="unfinished">Administrator OK</translation>
    </message>
    <message>
        <source>Content Version</source>
        <translation>Inhaltsversion</translation>
    </message>
    <message>
        <source>Try Another CM</source>
        <translation>Versuch ein anderen CM</translation>
    </message>
    <message>
        <source>Password Required To Kick Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already Logged In Elsewhere</source>
        <translation>Bereits woanders eingeloggt</translation>
    </message>
    <message>
        <source>Suspended</source>
        <translation>Gesperrt</translation>
    </message>
    <message>
        <source>Cancelled</source>
        <translation>Abgebrochen</translation>
    </message>
    <message>
        <source>Data Corruption</source>
        <translation>Datenkorruption</translation>
    </message>
    <message>
        <source>Disk Full</source>
        <translation>Festplatte voll</translation>
    </message>
    <message>
        <source>Remote Call Failed</source>
        <translation type="unfinished">Remote Call Failed</translation>
    </message>
    <message>
        <source>Password Unset</source>
        <translation>Passwort aufheben</translation>
    </message>
    <message>
        <source>External Account Unlinked</source>
        <translation>Externe Accountverknüpfung aufheben</translation>
    </message>
    <message>
        <source>PSN Ticket Invalid</source>
        <translation>ungültiges PSN Ticket</translation>
    </message>
    <message>
        <source>External Account Already Linked</source>
        <translation>Externer Account ist bereits verbunden</translation>
    </message>
    <message>
        <source>Remote File Conflict</source>
        <translation>Entfernter Dateikonflikt</translation>
    </message>
    <message>
        <source>Illegal Password</source>
        <translation>Nicht unterstütztes Passwort</translation>
    </message>
    <message>
        <source>Same As Previous Value</source>
        <translation>Gleicher wert wie vorheriger</translation>
    </message>
    <message>
        <source>Account Logon Denied</source>
        <translation>Anmeldung verweigert</translation>
    </message>
    <message>
        <source>Cannot Use Old Password</source>
        <translation>Du kannst dein altes Passwort nicht benutzen</translation>
    </message>
    <message>
        <source>Invalid Login AuthCode</source>
        <translation>Falscher Login AuthCode</translation>
    </message>
    <message>
        <source>Account Logon Denied No Mail</source>
        <translation>Account anmeldung abgelehnt, keine Mail</translation>
    </message>
    <message>
        <source>Hardware Not Capable Of IPT</source>
        <translation>Hardware ist nicht IPT fähig</translation>
    </message>
    <message>
        <source>IPT Init Error</source>
        <translation>IPT Init Fehler</translation>
    </message>
    <message>
        <source>Parental Control Restricted</source>
        <translation>Kindersicherung eingeschränkt</translation>
    </message>
    <message>
        <source>Facebook Query Error</source>
        <translation>Facebook-Abfragefehler</translation>
    </message>
    <message>
        <source>Expired Login Auth Code</source>
        <translation>Abgelaufener Login Auth Code</translation>
    </message>
    <message>
        <source>IP Login Restriction Failed</source>
        <translation>IP-Anmeldebeschränkung fehlgeschlagen</translation>
    </message>
    <message>
        <source>Account Locked Down</source>
        <translation>Account gesperrt</translation>
    </message>
    <message>
        <source>Account Logon Denied Verified Email Required</source>
        <translation>Account anmeldung abgelehnt, Verifizierte E-Mail erforderlich</translation>
    </message>
    <message>
        <source>No MatchingURL</source>
        <translation>Keine passende URL</translation>
    </message>
    <message>
        <source>Bad Response</source>
        <translation>Ungültige Antwort</translation>
    </message>
    <message>
        <source>Require Password ReEntry</source>
        <translation>Passwort erneurt eintragen</translation>
    </message>
    <message>
        <source>Value Out Of Range</source>
        <translation>Wert außerhalb des Bereichs</translation>
    </message>
    <message>
        <source>Unexpecte Error</source>
        <translation>Unerwarteter Fehler</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <source>Invalid CEG Submission</source>
        <translation>Ungültige CEG-Einreichung</translation>
    </message>
    <message>
        <source>Restricted Device</source>
        <translation>Eingeschränktes Gerät</translation>
    </message>
    <message>
        <source>Region Locked</source>
        <translation>Region gesperrt</translation>
    </message>
    <message>
        <source>Rate Limit Exceeded</source>
        <translation>Frequenzgrenze überschritten</translation>
    </message>
    <message>
        <source>Account Login Denied Need Two Factor</source>
        <translation>Accountanmeldung verweigert zwei Faktor Auth benötigt</translation>
    </message>
    <message>
        <source>Item Deleted</source>
        <translation>item gelöscht</translation>
    </message>
    <message>
        <source>Account Login Denied Throttle</source>
        <translation>Account Anmeldung abgelehnt</translation>
    </message>
    <message>
        <source>Two Factor Code Mismatch</source>
        <translation>Zwei Faktor Code stimmt nicht überein</translation>
    </message>
    <message>
        <source>Two Factor Activation Code Mismatch</source>
        <translation>Zwei Faktor Code stimmt nicht überein</translation>
    </message>
    <message>
        <source>Account Associated To Multiple Partners</source>
        <translation>Account ist mehreren Partnern zugeordnet</translation>
    </message>
    <message>
        <source>Not Modified</source>
        <translation>Nicht modifiziert</translation>
    </message>
    <message>
        <source>No Mobile Device</source>
        <translation>Kein Mobilgerät</translation>
    </message>
    <message>
        <source>Time Not Synced</source>
        <translation>Zeit nicht synchronisiert</translation>
    </message>
    <message>
        <source>Sms Code Failed</source>
        <translation>SMS-Code fehlgeschlagen</translation>
    </message>
    <message>
        <source>Account Limit Exceeded</source>
        <translation>Kontolimit überschritten</translation>
    </message>
    <message>
        <source>Account Activity Limit Exceeded</source>
        <translation>Kontoaktivitätslimit überschritten</translation>
    </message>
    <message>
        <source>Phone Activity Limit Exceeded</source>
        <translation>Telefonaktivitätslimit überschritten</translation>
    </message>
    <message>
        <source>Refund To Wallet</source>
        <translation>Rückerstattung an Wallet</translation>
    </message>
    <message>
        <source>Email Send Failure</source>
        <translation>E-Mail-Sendefehler</translation>
    </message>
    <message>
        <source>Not Settled</source>
        <translation>Nicht geklärt</translation>
    </message>
    <message>
        <source>Need Captcha</source>
        <translation>Captcha benötigt</translation>
    </message>
    <message>
        <source>GSLT Denied</source>
        <translation>GSLT abgelehnt</translation>
    </message>
    <message>
        <source>GS Owner Denied</source>
        <translation>GS besitzer abgelehnt</translation>
    </message>
    <message>
        <source>Invalid Item Type</source>
        <translation>Ungültiger Item Typ</translation>
    </message>
    <message>
        <source>IP Banned</source>
        <translation>IP Gebannt</translation>
    </message>
    <message>
        <source>GSLT Expired</source>
        <translation>GSLT Abgelaufen</translation>
    </message>
    <message>
        <source>Insufficient Funds</source>
        <translation>Unzureichende Mittel</translation>
    </message>
    <message>
        <source>Too Many Pending</source>
        <translation>Zu viele ausstehend</translation>
    </message>
    <message>
        <source>No Site Licenses Found</source>
        <translation>Keine Site-Lizenzen gefunden</translation>
    </message>
    <message>
        <source>WG Network Send Exceeded</source>
        <translation>WG-Netzwerk senden überschritten</translation>
    </message>
    <message>
        <source>Account Not Friends</source>
        <translation>Account nicht befreundet</translation>
    </message>
    <message>
        <source>Limited User Account</source>
        <translation>Eingeschränktes Benutzerkonto</translation>
    </message>
    <message>
        <source>Cant Remove Item</source>
        <translation>Item kann nicht Entfernt werden</translation>
    </message>
    <message>
        <source>Account Deleted</source>
        <translation>Account gelöscht</translation>
    </message>
    <message>
        <source>Existing User Cancelled License</source>
        <translation>Benutzer hat Lizenz Annulliert</translation>
    </message>
    <message>
        <source>Community Cooldown</source>
        <translation>Community-Abklingzeit</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation>Status:</translation>
    </message>
    <message>
        <source>Upload Progress: </source>
        <translation>Upload-Fortschritt: </translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation>Erstelle ein Website Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Wallpaper Name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Schlagwörter</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Vorschaubild</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation>Speichern...</translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation>Erfolgreich Steam Workshop Item Abonniert!</translation>
    </message>
    <message>
        <source>Download complete!</source>
        <translation>Download abgeschlossen!</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation>Neuigkeiten &amp; Patchnotes</translation>
    </message>
</context>
</TS>
