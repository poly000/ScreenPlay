import QtQuick
import QtQuick.Controls.Material
import ScreenPlay 1.0

Text {
    text: qsTr("Headline Section")
    font.pointSize: 14
    color: Material.primaryTextColor
    font.family: ScreenPlay.settings.font
}
