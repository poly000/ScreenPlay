<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_BR" sourcelanguage="en">
<context>
    <name>ColorPicker</name>
    <message>
        <source>Red</source>
        <translation>Vermelho</translation>
    </message>
    <message>
        <source>Green</source>
        <translation>Verde</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation>Azul</translation>
    </message>
    <message>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <source>HSV</source>
        <translation>HSV</translation>
    </message>
    <message>
        <source>R:</source>
        <translation>R:</translation>
    </message>
    <message>
        <source>G:</source>
        <translation>G:</translation>
    </message>
    <message>
        <source>B:</source>
        <translation>B:</translation>
    </message>
    <message>
        <source>H:</source>
        <translation type="unfinished">H:</translation>
    </message>
    <message>
        <source>S:</source>
        <translation type="unfinished">S:</translation>
    </message>
    <message>
        <source>V:</source>
        <translation type="unfinished">V:</translation>
    </message>
    <message>
        <source>Alpha:</source>
        <translation>Alpha:</translation>
    </message>
    <message>
        <source>#</source>
        <translation>#</translation>
    </message>
</context>
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Novidades</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Fórum</translation>
    </message>
    <message>
        <source>Issue List</source>
        <translation>Lista de Issues</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Contribuir</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Oficina Steam</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Abrir no navegador</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Importar qualquer tipo de vídeo</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>Dependendo da configuração do seu PC, é melhor converter seu papel de parede para um codec de vídeo específico. Se ambos tiverem desempenho ruim, você também pode tentar um papel de parede QML! Formatos de vídeo suportados são: 

*. p4 *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Defina o codec de vídeo de sua escolha:</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Controle de qualidade. Menor valor significa melhor qualidade.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Abrir documentação</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Selecionar arquivo</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Um erro ocorreu!</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Copiar texto para a área de transferência</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Voltar para criar e enviar um relatório de erros!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Gerando pré-visualização...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Gerando pré-visualização de miniaturas...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Gerando pré-visualização de vídeo de 5 segundos...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Gerando gif de pré-visualização...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Convertendo áudio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Convertendo vídeo... Isso pode levar algum tempo!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Erro de conversão de vídeo!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Erro de análise de vídeo!</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Converter um vídeo para um plano de fundo</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Gerando vídeo de pré-visualização...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Nome (obrigatório!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Descrição</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>URL do Youtube</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Abortar</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Salvar</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Salvar o papel de parede...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Volume</translation>
    </message>
    <message>
        <source>Playback rate</source>
        <translation>Taxa de reprodução</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Tempo atual do vídeo</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Modo de preenchimento</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Esticar</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Preencher</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Contém</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Capa</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>Escalonar_Abaixo</translation>
    </message>
</context>
<context>
    <name>FileSelector</name>
    <message>
        <source>Clear</source>
        <translation>Limpar</translation>
    </message>
    <message>
        <source>Select File</source>
        <translation>Selecionar arquivo</translation>
    </message>
    <message>
        <source>Please choose a file</source>
        <translation>Por favor, escolha um arquivo</translation>
    </message>
</context>
<context>
    <name>Forum</name>
    <message>
        <source>Download Wallpaper and Widgets from our forums manually. If you want to download Steam Workshop content you have to install ScreenPlay via Steam.</source>
        <translation>Baixe papéis de parede e widgets manualmente através de nossos fóruns. Se você quiser baixar conteúdo da Oficina Steam, você precisa instalar ScreenPlay via Steam.</translation>
    </message>
    <message>
        <source>Install Steam Version</source>
        <translation type="unfinished">Install Steam Version</translation>
    </message>
    <message>
        <source>Open In Browser</source>
        <translation type="unfinished">Open In Browser</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation type="unfinished">Import a Gif Wallpaper</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished">Drop a *.gif file here or use &apos;Select file&apos; below.</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation type="unfinished">Select your gif</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished">General</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation type="unfinished">Wallpaper name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation type="unfinished">Created By</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="unfinished">Tags</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation type="unfinished">Create a HTML Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished">General</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation type="unfinished">Wallpaper name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation type="unfinished">Created By</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation type="unfinished">License &amp; Tags</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation type="unfinished">Preview Image</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation type="unfinished">Create a HTML widget</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished">General</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation type="unfinished">Widget name</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation type="unfinished">Created by</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="unfinished">Tags</translation>
    </message>
</context>
<context>
    <name>HeadlineSection</name>
    <message>
        <source>Headline Section</source>
        <translation type="unfinished">Headline Section</translation>
    </message>
</context>
<context>
    <name>ImageSelector</name>
    <message>
        <source>Set your own preview image</source>
        <translation type="unfinished">Set your own preview image</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="unfinished">Clear</translation>
    </message>
    <message>
        <source>Select Preview Image</source>
        <translation type="unfinished">Select Preview Image</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished">AnalyseVideo...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished">Generating preview image...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Generating preview thumbnail image...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Generating 5 second preview video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Generating preview gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished">Converting Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Converting Video... This can take some time!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Converting Video ERROR!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished">Analyse Video ERROR!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Import a video to a wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished">Generating preview video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished">Name (required!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished">Youtube URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished">Abort</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Save Wallpaper...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation type="unfinished">Import a .webm video</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation type="unfinished">When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation type="unfinished">Invalid file type. Must be valid VP8 or VP9 (*.webm)!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished">Drop a *.webm file here or use &apos;Select file&apos; below.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished">Open Documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Selecionar arquivo</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished">AnalyseVideo...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Import a video to a wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished">Selecionar arquivo</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Atualizando!</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation type="unfinished">Pull to refresh!</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Obtenha mais papéis de parede e Widgets através da Oficina Steam!</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>Abrir a pasta</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Remover Item</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Remover da Oficina</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Abrir Página da Oficina</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Tem certeza que deseja remover este item?</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Obtenha widgets e papéis de parede gratuitos através da Oficina Steam</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Navegar na Oficina Steam</translation>
    </message>
</context>
<context>
    <name>LicenseSelector</name>
    <message>
        <source>License</source>
        <translation>Licença</translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material for any purpose, even commercially.</source>
        <translation>Share — copie e redistribua o material por qualquer meio ou formato. Adapte — modifique, transforme e desenvolva o material para qualquer propósito, mesmo comercialmente.</translation>
    </message>
    <message>
        <source>You grant other to remix your work and change the license to their liking.</source>
        <translation>Você permite que outros modifiquem seu trabalho e alterem a licença livremente.</translation>
    </message>
    <message>
        <source>Share — copy and redistribute the material in any medium or format. Adapt — remix, transform, and build upon the material. You are not allowed to use it commercially! </source>
        <translation>Share — copie e redistribua o material por qualquer meio ou formato. Adapte — modifique, transforme e desenvolva o material. Você não está permitido a usar isso comercialmente! </translation>
    </message>
    <message>
        <source>You allow everyone to do anything with your work.</source>
        <translation>Você permite que todos façam qualquer coisa com seu trabalho.</translation>
    </message>
    <message>
        <source>You grant other to remix your work but it must remain under the GPLv3. We recommend this license for all code wallpaper!</source>
        <translation type="unfinished">You grant other to remix your work but it must remain under the GPLv3. We recommend this license for all code wallpaper!</translation>
    </message>
    <message>
        <source>You do not share any rights and nobody is allowed to use or remix it (Not recommended). Can also used to credit work others.</source>
        <translation type="unfinished">You do not share any rights and nobody is allowed to use or remix it (Not recommended). Can also used to credit work others.</translation>
    </message>
</context>
<context>
    <name>MonitorConfiguration</name>
    <message>
        <source>Your monitor setup changed!
 Please configure your wallpaper again.</source>
        <translation type="unfinished">Your monitor setup changed!
 Please configure your wallpaper again.</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation type="unfinished">Wallpaper Configuration</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation type="unfinished">Remove selected</translation>
    </message>
    <message>
        <source>Remove </source>
        <translation type="unfinished">Remove </translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation type="unfinished">Wallpapers</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation type="unfinished">Set color</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation type="unfinished">Please choose a color</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>All</source>
        <translation type="unfinished">All</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="unfinished">Scenes</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
    <message>
        <source>Install Date Ascending</source>
        <translation type="unfinished">Install Date Ascending</translation>
    </message>
    <message>
        <source>Install Date Descending</source>
        <translation type="unfinished">Install Date Descending</translation>
    </message>
    <message>
        <source> Subscribed items: </source>
        <translation type="unfinished"> Subscribed items: </translation>
    </message>
    <message>
        <source>Upload to the Steam Workshop</source>
        <translation type="unfinished">Upload to the Steam Workshop</translation>
    </message>
    <message>
        <source>Create</source>
        <translation type="unfinished">Create</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation type="unfinished">Workshop</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation type="unfinished">Installed</translation>
    </message>
    <message>
        <source>Community</source>
        <translation type="unfinished">Community</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished">Settings</translation>
    </message>
</context>
<context>
    <name>NavigationWallpaperConfiguration</name>
    <message>
        <source>Configurate active Wallpaper or Widgets</source>
        <translation type="unfinished">Configurate active Wallpaper or Widgets</translation>
    </message>
    <message>
        <source>No active Wallpaper or Widgets</source>
        <translation type="unfinished">No active Wallpaper or Widgets</translation>
    </message>
</context>
<context>
    <name>PopupOffline</name>
    <message>
        <source>You need to run Steam for this. steamErrorRestart: %1 - steamErrorAPIInit: %2</source>
        <translation type="unfinished">You need to run Steam for this. steamErrorRestart: %1 - steamErrorAPIInit: %2</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="unfinished">Back</translation>
    </message>
</context>
<context>
    <name>PopupSteamWorkshopAgreement</name>
    <message>
        <source>You Need to Agree To The Steam Subscriber Agreement First</source>
        <translation type="unfinished">You Need to Agree To The Steam Subscriber Agreement First</translation>
    </message>
    <message>
        <source>REQUIRES INTERNET CONNECTION AND FREE STEAM ACCOUNT TO ACTIVATE. Notice: Product offered subject to your acceptance of the Steam Subscriber Agreement (SSA). You must activate this product via the Internet by registering for a Steam account and accepting the SSA. Please see https://store.steampowered.com/subscriber_agreement/ to view the SSA prior to purchase. If you do not agree with the provisions of the SSA, you should return this game unopened to your retailer in accordance with their return policy.</source>
        <translation type="unfinished">REQUIRES INTERNET CONNECTION AND FREE STEAM ACCOUNT TO ACTIVATE. Notice: Product offered subject to your acceptance of the Steam Subscriber Agreement (SSA). You must activate this product via the Internet by registering for a Steam account and accepting the SSA. Please see https://store.steampowered.com/subscriber_agreement/ to view the SSA prior to purchase. If you do not agree with the provisions of the SSA, you should return this game unopened to your retailer in accordance with their return policy.</translation>
    </message>
    <message>
        <source>View The Steam Subscriber Agreement</source>
        <translation type="unfinished">View The Steam Subscriber Agreement</translation>
    </message>
    <message>
        <source>Accept Steam Workshop Agreement</source>
        <translation type="unfinished">Accept Steam Workshop Agreement</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation type="unfinished">Create a QML Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished">General</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation type="unfinished">Wallpaper name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation type="unfinished">Created By</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation type="unfinished">License &amp; Tags</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation type="unfinished">Preview Image</translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation type="unfinished">Create a QML widget</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished">General</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation type="unfinished">Widget name</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation type="unfinished">Created by</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="unfinished">Tags</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation type="unfinished">Profile saved successfully!</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation type="unfinished">NEW</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <source>Search for Wallpaper &amp; Widgets</source>
        <translation type="unfinished">Search for Wallpaper &amp; Widgets</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation type="unfinished">General</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation type="unfinished">Autostart</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation type="unfinished">ScreenPlay will start with Windows and will setup your Desktop every time for you.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation type="unfinished">High priority Autostart</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation type="unfinished">This options grants ScreenPlay a higher autostart priority than other apps.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation type="unfinished">Send anonymous crash reports and statistics</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation type="unfinished">Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation type="unfinished">Set save location</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation type="unfinished">Set location</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation type="unfinished">Your storage path is empty!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation type="unfinished">Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</translation>
    </message>
    <message>
        <source>Language</source>
        <translation type="unfinished">Language</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation type="unfinished">Set the ScreenPlay UI Language</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished">Theme</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation type="unfinished">Switch dark/light theme</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation type="unfinished">System Default</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="unfinished">Dark</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="unfinished">Light</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation type="unfinished">Performance</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation type="unfinished">Pause wallpaper video rendering while another app is in the foreground</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation type="unfinished">We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation type="unfinished">Default Fill Mode</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation type="unfinished">Set this property to define how the video is scaled to fit the target area.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation type="unfinished">Stretch</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation type="unfinished">Fill</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation type="unfinished">Contain</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="unfinished">Cover</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation type="unfinished">Scale-Down</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">About</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation type="unfinished">Thank you for using ScreenPlay</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation type="unfinished">Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="unfinished">Version</translation>
    </message>
    <message>
        <source>ScreenPlay Build Version </source>
        <translation type="obsolete">ScreenPlay Build Version </translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation type="unfinished">Open Changelog</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation type="unfinished">Third Party Software</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation type="unfinished">ScreenPlay would not be possible without the work of others. A big thank you to: </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation type="unfinished">Licenses</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation type="unfinished">Logs</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation type="unfinished">If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation type="unfinished">Show Logs</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation type="unfinished">Data Protection</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation type="unfinished">We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation type="unfinished">Privacy</translation>
    </message>
    <message>
        <source>ScreenPlay Build Version 
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation type="unfinished">Copy text to clipboard</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation type="unfinished">Tools Overview</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation type="unfinished">GIF Wallpaper</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation type="unfinished">QML Wallpaper</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation type="unfinished">HTML5 Wallpaper</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation type="unfinished">Website Wallpaper</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation type="unfinished">QML Widget</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation type="unfinished">HTML Widget</translation>
    </message>
    <message>
        <source>Set Wallpaper</source>
        <translation type="unfinished">Set Wallpaper</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation type="unfinished">Set Widget</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation type="unfinished">Headline</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation type="unfinished">Select a Monitor to display the content</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation type="unfinished">Set Volume</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation type="unfinished">Fill Mode</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation type="unfinished">Stretch</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation type="unfinished">Fill</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation type="unfinished">Contain</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation type="unfinished">Cover</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation type="unfinished">Scale-Down</translation>
    </message>
    <message>
        <source>Size: </source>
        <translation type="unfinished">Size: </translation>
    </message>
    <message>
        <source>No description...</source>
        <translation type="unfinished">No description...</translation>
    </message>
    <message>
        <source>Click here if you like the content</source>
        <translation type="unfinished">Click here if you like the content</translation>
    </message>
    <message>
        <source>Click here if you do not like the content</source>
        <translation type="unfinished">Click here if you do not like the content</translation>
    </message>
    <message>
        <source>Subscribtions: </source>
        <translation type="unfinished">Subscribtions: </translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation type="unfinished">Open In Steam</translation>
    </message>
    <message>
        <source>Subscribed!</source>
        <translation type="unfinished">Subscribed!</translation>
    </message>
    <message>
        <source>Subscribe</source>
        <translation type="unfinished">Subscribe</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SteamNotAvailable</name>
    <message>
        <source>Could not load steam integration!</source>
        <translation type="unfinished">Could not load steam integration!</translation>
    </message>
</context>
<context>
    <name>SteamProfile</name>
    <message>
        <source>Back</source>
        <translation type="unfinished">Back</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="unfinished">Forward</translation>
    </message>
</context>
<context>
    <name>SteamWorkshopStartPage</name>
    <message>
        <source>Loading</source>
        <translation type="unfinished">Loading</translation>
    </message>
    <message>
        <source>Download now!</source>
        <translation type="unfinished">Download now!</translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation type="unfinished">Downloading...</translation>
    </message>
    <message>
        <source>Details</source>
        <translation type="unfinished">Details</translation>
    </message>
    <message>
        <source>Open In Steam</source>
        <translation type="unfinished">Open In Steam</translation>
    </message>
    <message>
        <source>Profile</source>
        <translation type="unfinished">Profile</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation type="unfinished">Upload</translation>
    </message>
    <message>
        <source>Search for Wallpaper and Widgets...</source>
        <translation type="unfinished">Search for Wallpaper and Widgets...</translation>
    </message>
    <message>
        <source>Open Workshop in Steam</source>
        <translation type="unfinished">Open Workshop in Steam</translation>
    </message>
    <message>
        <source>Ranked By Vote</source>
        <translation type="unfinished">Ranked By Vote</translation>
    </message>
    <message>
        <source>Publication Date</source>
        <translation type="unfinished">Publication Date</translation>
    </message>
    <message>
        <source>Ranked By Trend</source>
        <translation type="unfinished">Ranked By Trend</translation>
    </message>
    <message>
        <source>Favorited By Friends</source>
        <translation type="unfinished">Favorited By Friends</translation>
    </message>
    <message>
        <source>Created By Friends</source>
        <translation type="unfinished">Created By Friends</translation>
    </message>
    <message>
        <source>Created By Followed Users</source>
        <translation type="unfinished">Created By Followed Users</translation>
    </message>
    <message>
        <source>Not Yet Rated</source>
        <translation type="unfinished">Not Yet Rated</translation>
    </message>
    <message>
        <source>Total VotesAsc</source>
        <translation type="unfinished">Total VotesAsc</translation>
    </message>
    <message>
        <source>Votes Up</source>
        <translation type="unfinished">Votes Up</translation>
    </message>
    <message>
        <source>Total Unique Subscriptions</source>
        <translation type="unfinished">Total Unique Subscriptions</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="unfinished">Back</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation type="unfinished">Forward</translation>
    </message>
</context>
<context>
    <name>TagSelector</name>
    <message>
        <source>Save</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <source>Add tag</source>
        <translation type="unfinished">Add tag</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">Cancel</translation>
    </message>
    <message>
        <source>Add Tag</source>
        <translation type="unfinished">Add Tag</translation>
    </message>
</context>
<context>
    <name>TextField</name>
    <message>
        <source>Label</source>
        <translation type="unfinished">Label</translation>
    </message>
    <message>
        <source>*Required</source>
        <translation type="unfinished">*Required</translation>
    </message>
</context>
<context>
    <name>TrayIcon</name>
    <message>
        <source>ScreenPlay - Double click to change you settings.</source>
        <translation type="unfinished">ScreenPlay - Double click to change you settings.</translation>
    </message>
    <message>
        <source>Open ScreenPlay</source>
        <translation type="unfinished">Open ScreenPlay</translation>
    </message>
    <message>
        <source>Mute all</source>
        <translation type="unfinished">Mute all</translation>
    </message>
    <message>
        <source>Unmute all</source>
        <translation type="unfinished">Unmute all</translation>
    </message>
    <message>
        <source>Pause all</source>
        <translation type="unfinished">Pause all</translation>
    </message>
    <message>
        <source>Play all</source>
        <translation type="unfinished">Play all</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="unfinished">Quit</translation>
    </message>
</context>
<context>
    <name>UploadProject</name>
    <message>
        <source>Upload Wallpaper/Widgets to Steam</source>
        <translation type="unfinished">Upload Wallpaper/Widgets to Steam</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished">Abort</translation>
    </message>
    <message>
        <source>Upload Selected Projects</source>
        <translation type="unfinished">Upload Selected Projects</translation>
    </message>
    <message>
        <source>Finish</source>
        <translation type="unfinished">Finish</translation>
    </message>
</context>
<context>
    <name>UploadProjectBigItem</name>
    <message>
        <source>Type: </source>
        <translation type="unfinished">Type: </translation>
    </message>
    <message>
        <source>Open Folder</source>
        <translation type="unfinished">Open Folder</translation>
    </message>
    <message>
        <source>Invalid Project!</source>
        <translation type="unfinished">Invalid Project!</translation>
    </message>
</context>
<context>
    <name>UploadProjectItem</name>
    <message>
        <source>Fail</source>
        <translation type="unfinished">Fail</translation>
    </message>
    <message>
        <source>No Connection</source>
        <translation type="unfinished">No Connection</translation>
    </message>
    <message>
        <source>Invalid Password</source>
        <translation type="unfinished">Invalid Password</translation>
    </message>
    <message>
        <source>Logged In Elsewhere</source>
        <translation type="unfinished">Logged In Elsewhere</translation>
    </message>
    <message>
        <source>Invalid Protocol Version</source>
        <translation type="unfinished">Invalid Protocol Version</translation>
    </message>
    <message>
        <source>Invalid Param</source>
        <translation type="unfinished">Invalid Param</translation>
    </message>
    <message>
        <source>File Not Found</source>
        <translation type="unfinished">File Not Found</translation>
    </message>
    <message>
        <source>Busy</source>
        <translation type="unfinished">Busy</translation>
    </message>
    <message>
        <source>Invalid State</source>
        <translation type="unfinished">Invalid State</translation>
    </message>
    <message>
        <source>Invalid Name</source>
        <translation type="unfinished">Invalid Name</translation>
    </message>
    <message>
        <source>Invalid Email</source>
        <translation type="unfinished">Invalid Email</translation>
    </message>
    <message>
        <source>Duplicate Name</source>
        <translation type="unfinished">Duplicate Name</translation>
    </message>
    <message>
        <source>Access Denied</source>
        <translation type="unfinished">Access Denied</translation>
    </message>
    <message>
        <source>Timeout</source>
        <translation type="unfinished">Timeout</translation>
    </message>
    <message>
        <source>Banned</source>
        <translation type="unfinished">Banned</translation>
    </message>
    <message>
        <source>Account Not Found</source>
        <translation type="unfinished">Account Not Found</translation>
    </message>
    <message>
        <source>Invalid SteamID</source>
        <translation type="unfinished">Invalid SteamID</translation>
    </message>
    <message>
        <source>Service Unavailable</source>
        <translation type="unfinished">Service Unavailable</translation>
    </message>
    <message>
        <source>Not Logged On</source>
        <translation type="unfinished">Not Logged On</translation>
    </message>
    <message>
        <source>Pending</source>
        <translation type="unfinished">Pending</translation>
    </message>
    <message>
        <source>Encryption Failure</source>
        <translation type="unfinished">Encryption Failure</translation>
    </message>
    <message>
        <source>Insufficient Privilege</source>
        <translation type="unfinished">Insufficient Privilege</translation>
    </message>
    <message>
        <source>Limit Exceeded</source>
        <translation type="unfinished">Limit Exceeded</translation>
    </message>
    <message>
        <source>Revoked</source>
        <translation type="unfinished">Revoked</translation>
    </message>
    <message>
        <source>Expired</source>
        <translation type="unfinished">Expired</translation>
    </message>
    <message>
        <source>Already Redeemed</source>
        <translation type="unfinished">Already Redeemed</translation>
    </message>
    <message>
        <source>Duplicate Request</source>
        <translation type="unfinished">Duplicate Request</translation>
    </message>
    <message>
        <source>Already Owned</source>
        <translation type="unfinished">Already Owned</translation>
    </message>
    <message>
        <source>IP Not Found</source>
        <translation type="unfinished">IP Not Found</translation>
    </message>
    <message>
        <source>Persist Failed</source>
        <translation type="unfinished">Persist Failed</translation>
    </message>
    <message>
        <source>Locking Failed</source>
        <translation type="unfinished">Locking Failed</translation>
    </message>
    <message>
        <source>Logon Session Replaced</source>
        <translation type="unfinished">Logon Session Replaced</translation>
    </message>
    <message>
        <source>Connect Failed</source>
        <translation type="unfinished">Connect Failed</translation>
    </message>
    <message>
        <source>Handshake Failed</source>
        <translation type="unfinished">Handshake Failed</translation>
    </message>
    <message>
        <source>IO Failure</source>
        <translation type="unfinished">IO Failure</translation>
    </message>
    <message>
        <source>Remote Disconnect</source>
        <translation type="unfinished">Remote Disconnect</translation>
    </message>
    <message>
        <source>Shopping Cart Not Found</source>
        <translation type="unfinished">Shopping Cart Not Found</translation>
    </message>
    <message>
        <source>Blocked</source>
        <translation type="unfinished">Blocked</translation>
    </message>
    <message>
        <source>Ignored</source>
        <translation type="unfinished">Ignored</translation>
    </message>
    <message>
        <source>No Match</source>
        <translation type="unfinished">No Match</translation>
    </message>
    <message>
        <source>Account Disabled</source>
        <translation type="unfinished">Account Disabled</translation>
    </message>
    <message>
        <source>Service ReadOnly</source>
        <translation type="unfinished">Service ReadOnly</translation>
    </message>
    <message>
        <source>Account Not Featured</source>
        <translation type="unfinished">Account Not Featured</translation>
    </message>
    <message>
        <source>Administrator OK</source>
        <translation type="unfinished">Administrator OK</translation>
    </message>
    <message>
        <source>Content Version</source>
        <translation type="unfinished">Content Version</translation>
    </message>
    <message>
        <source>Try Another CM</source>
        <translation type="unfinished">Try Another CM</translation>
    </message>
    <message>
        <source>Password Required To Kick Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Already Logged In Elsewhere</source>
        <translation type="unfinished">Already Logged In Elsewhere</translation>
    </message>
    <message>
        <source>Suspended</source>
        <translation type="unfinished">Suspended</translation>
    </message>
    <message>
        <source>Cancelled</source>
        <translation type="unfinished">Cancelled</translation>
    </message>
    <message>
        <source>Data Corruption</source>
        <translation type="unfinished">Data Corruption</translation>
    </message>
    <message>
        <source>Disk Full</source>
        <translation type="unfinished">Disk Full</translation>
    </message>
    <message>
        <source>Remote Call Failed</source>
        <translation type="unfinished">Remote Call Failed</translation>
    </message>
    <message>
        <source>Password Unset</source>
        <translation type="unfinished">Password Unset</translation>
    </message>
    <message>
        <source>External Account Unlinked</source>
        <translation type="unfinished">External Account Unlinked</translation>
    </message>
    <message>
        <source>PSN Ticket Invalid</source>
        <translation type="unfinished">PSN Ticket Invalid</translation>
    </message>
    <message>
        <source>External Account Already Linked</source>
        <translation type="unfinished">External Account Already Linked</translation>
    </message>
    <message>
        <source>Remote File Conflict</source>
        <translation type="unfinished">Remote File Conflict</translation>
    </message>
    <message>
        <source>Illegal Password</source>
        <translation type="unfinished">Illegal Password</translation>
    </message>
    <message>
        <source>Same As Previous Value</source>
        <translation type="unfinished">Same As Previous Value</translation>
    </message>
    <message>
        <source>Account Logon Denied</source>
        <translation type="unfinished">Account Logon Denied</translation>
    </message>
    <message>
        <source>Cannot Use Old Password</source>
        <translation type="unfinished">Cannot Use Old Password</translation>
    </message>
    <message>
        <source>Invalid Login AuthCode</source>
        <translation type="unfinished">Invalid Login AuthCode</translation>
    </message>
    <message>
        <source>Account Logon Denied No Mail</source>
        <translation type="unfinished">Account Logon Denied No Mail</translation>
    </message>
    <message>
        <source>Hardware Not Capable Of IPT</source>
        <translation type="unfinished">Hardware Not Capable Of IPT</translation>
    </message>
    <message>
        <source>IPT Init Error</source>
        <translation type="unfinished">IPT Init Error</translation>
    </message>
    <message>
        <source>Parental Control Restricted</source>
        <translation type="unfinished">Parental Control Restricted</translation>
    </message>
    <message>
        <source>Facebook Query Error</source>
        <translation type="unfinished">Facebook Query Error</translation>
    </message>
    <message>
        <source>Expired Login Auth Code</source>
        <translation type="unfinished">Expired Login Auth Code</translation>
    </message>
    <message>
        <source>IP Login Restriction Failed</source>
        <translation type="unfinished">IP Login Restriction Failed</translation>
    </message>
    <message>
        <source>Account Locked Down</source>
        <translation type="unfinished">Account Locked Down</translation>
    </message>
    <message>
        <source>Account Logon Denied Verified Email Required</source>
        <translation type="unfinished">Account Logon Denied Verified Email Required</translation>
    </message>
    <message>
        <source>No MatchingURL</source>
        <translation type="unfinished">No MatchingURL</translation>
    </message>
    <message>
        <source>Bad Response</source>
        <translation type="unfinished">Bad Response</translation>
    </message>
    <message>
        <source>Require Password ReEntry</source>
        <translation type="unfinished">Require Password ReEntry</translation>
    </message>
    <message>
        <source>Value Out Of Range</source>
        <translation type="unfinished">Value Out Of Range</translation>
    </message>
    <message>
        <source>Unexpecte Error</source>
        <translation type="unfinished">Unexpecte Error</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="unfinished">Disabled</translation>
    </message>
    <message>
        <source>Invalid CEG Submission</source>
        <translation type="unfinished">Invalid CEG Submission</translation>
    </message>
    <message>
        <source>Restricted Device</source>
        <translation type="unfinished">Restricted Device</translation>
    </message>
    <message>
        <source>Region Locked</source>
        <translation type="unfinished">Region Locked</translation>
    </message>
    <message>
        <source>Rate Limit Exceeded</source>
        <translation type="unfinished">Rate Limit Exceeded</translation>
    </message>
    <message>
        <source>Account Login Denied Need Two Factor</source>
        <translation type="unfinished">Account Login Denied Need Two Factor</translation>
    </message>
    <message>
        <source>Item Deleted</source>
        <translation type="unfinished">Item Deleted</translation>
    </message>
    <message>
        <source>Account Login Denied Throttle</source>
        <translation type="unfinished">Account Login Denied Throttle</translation>
    </message>
    <message>
        <source>Two Factor Code Mismatch</source>
        <translation type="unfinished">Two Factor Code Mismatch</translation>
    </message>
    <message>
        <source>Two Factor Activation Code Mismatch</source>
        <translation type="unfinished">Two Factor Activation Code Mismatch</translation>
    </message>
    <message>
        <source>Account Associated To Multiple Partners</source>
        <translation type="unfinished">Account Associated To Multiple Partners</translation>
    </message>
    <message>
        <source>Not Modified</source>
        <translation type="unfinished">Not Modified</translation>
    </message>
    <message>
        <source>No Mobile Device</source>
        <translation type="unfinished">No Mobile Device</translation>
    </message>
    <message>
        <source>Time Not Synced</source>
        <translation type="unfinished">Time Not Synced</translation>
    </message>
    <message>
        <source>Sms Code Failed</source>
        <translation type="unfinished">Sms Code Failed</translation>
    </message>
    <message>
        <source>Account Limit Exceeded</source>
        <translation type="unfinished">Account Limit Exceeded</translation>
    </message>
    <message>
        <source>Account Activity Limit Exceeded</source>
        <translation type="unfinished">Account Activity Limit Exceeded</translation>
    </message>
    <message>
        <source>Phone Activity Limit Exceeded</source>
        <translation type="unfinished">Phone Activity Limit Exceeded</translation>
    </message>
    <message>
        <source>Refund To Wallet</source>
        <translation type="unfinished">Refund To Wallet</translation>
    </message>
    <message>
        <source>Email Send Failure</source>
        <translation type="unfinished">Email Send Failure</translation>
    </message>
    <message>
        <source>Not Settled</source>
        <translation type="unfinished">Not Settled</translation>
    </message>
    <message>
        <source>Need Captcha</source>
        <translation type="unfinished">Need Captcha</translation>
    </message>
    <message>
        <source>GSLT Denied</source>
        <translation type="unfinished">GSLT Denied</translation>
    </message>
    <message>
        <source>GS Owner Denied</source>
        <translation type="unfinished">GS Owner Denied</translation>
    </message>
    <message>
        <source>Invalid Item Type</source>
        <translation type="unfinished">Invalid Item Type</translation>
    </message>
    <message>
        <source>IP Banned</source>
        <translation type="unfinished">IP Banned</translation>
    </message>
    <message>
        <source>GSLT Expired</source>
        <translation type="unfinished">GSLT Expired</translation>
    </message>
    <message>
        <source>Insufficient Funds</source>
        <translation type="unfinished">Insufficient Funds</translation>
    </message>
    <message>
        <source>Too Many Pending</source>
        <translation type="unfinished">Too Many Pending</translation>
    </message>
    <message>
        <source>No Site Licenses Found</source>
        <translation type="unfinished">No Site Licenses Found</translation>
    </message>
    <message>
        <source>WG Network Send Exceeded</source>
        <translation type="unfinished">WG Network Send Exceeded</translation>
    </message>
    <message>
        <source>Account Not Friends</source>
        <translation type="unfinished">Account Not Friends</translation>
    </message>
    <message>
        <source>Limited User Account</source>
        <translation type="unfinished">Limited User Account</translation>
    </message>
    <message>
        <source>Cant Remove Item</source>
        <translation type="unfinished">Cant Remove Item</translation>
    </message>
    <message>
        <source>Account Deleted</source>
        <translation type="unfinished">Account Deleted</translation>
    </message>
    <message>
        <source>Existing User Cancelled License</source>
        <translation type="unfinished">Existing User Cancelled License</translation>
    </message>
    <message>
        <source>Community Cooldown</source>
        <translation type="unfinished">Community Cooldown</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation type="unfinished">Status:</translation>
    </message>
    <message>
        <source>Upload Progress: </source>
        <translation type="unfinished">Upload Progress: </translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation type="unfinished">Create a Website Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="unfinished">General</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation type="unfinished">Wallpaper name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation type="unfinished">Created By</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation type="unfinished">Tags</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation type="unfinished">Preview Image</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation type="unfinished">Saving...</translation>
    </message>
</context>
<context>
    <name>WorkshopItem</name>
    <message>
        <source>Successfully subscribed to Workshop Item!</source>
        <translation type="unfinished">Successfully subscribed to Workshop Item!</translation>
    </message>
    <message>
        <source>Download complete!</source>
        <translation type="unfinished">Download complete!</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation type="unfinished">News &amp; Patchnotes</translation>
    </message>
</context>
</TS>
