import QtQuick
import Qt5Compat.GraphicalEffects
import QtQuick.Controls
import QtQuick.Controls.Material
import QtQuick.Layouts
import ScreenPlay 1.0
import ScreenPlay.Create 1.0

Item {
    id: root

    signal wizardStarted
    signal wizardExited
    signal next

    SwipeView {
        id: swipeView

        anchors.fill: parent
        interactive: false
        clip: true

        CreateWallpaperInit {
            onNext: (filePath, codec) => {
                         startConvert(filePath, codec)
                    }

        function startConvert(filePath, codec) {
            root.wizardStarted()
            swipeView.currentIndex = 1
            createWallpaperVideoImportConvert.codec = codec
            createWallpaperVideoImportConvert.filePath = filePath
            ScreenPlay.create.createWallpaperStart(filePath, codec, quality)
        }
    }

    CreateWallpaperVideoImportConvert {
        id: createWallpaperVideoImportConvert

        onAbort: root.wizardExited()
    }

    CreateWallpaperResult {}
}
}
